<?php
namespace Nsulistiyawan\Bpjs\Aplicare;

use Nsulistiyawan\Bpjs\BpjsService;

class KetersediaanKamar extends BpjsService
{

    public function refKelas()
    {
        $response = $this->get('aplicaresws/rest/ref/kelas');
        return json_decode($response, true);
    }
    public function bedGet($kodePpk, $start, $limit)
    {
        $response = $this->get('aplicaresws/rest/bed/read/'.$kodePpk.'/'.$start.'/'.$limit);
        return json_decode($response, true);
    }
    public function bedCreate($kodePpk, $data = [])
    {
        $header = 'application/json';
        $response = $this->post('aplicaresws/rest/bed/create/'.$kodePpk, $data, $header);
        return json_decode($response, true);
    }
    public function bedUpdate($kodePpk, $data = [])
    {
        $response = $this->put('aplicaresws/rest/bed/update/'.$kodePpk, $data);
        return json_decode($response, true);
    }
    public function bedDelete($kodePpk, $data = [])
    {
        $response = $this->delete('aplicaresws/rest/bed/delete/'.$kodePpk, $data);
        return json_decode($response, true);
    }
}