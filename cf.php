<?php
 		class Cf {

			var $_hostname = '172.16.200.5'; // 172.16.200.5 
			var $_database = 'rsud_dpk_17'; // rsud_dpk_17
			var $_username = 'daftar0l2'; // daftar0l
			var $_password = '#rsud99'; //#rsud99

			var $_hostname1 = 'localhost';
			var $_database1 = 'db_appcore';
			var $_username1 = 'root';
			var $_password1 = '';

			// Other
			var $_document_root	= '/opt/lampp/htdocs/android/';
			var $_file_akses	= 'http://172.16.200.5/appcore';
			var $_limit      	= array(10 => 10, 20 => 20, 50 => 50, 100 => 100);
			var $default_limit 	= 10;
			var $_jadwal_cuti   = 10;
			var $_hari_daftar	= '07';
			var $_limit_jam		= '16:00:00';

			var $_confbpjs_tes	= array(
				'cons_id'		=> 3362,
				'secret_key'	=> '1jY72ED457',
				'base_url' 		=> 'https://new-api.bpjs-kesehatan.go.id:8080',
				'service_name' 	=> 'new-vclaim-rest'
			);

			var $_confbpjs	= array(
				'cons_id'		=> 3362,
				'secret_key'	=> '1jY72ED457',
				'base_url' 		=> 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' 	=> 'vclaim-rest'
			);

			var $_confaplicare	= array(
				'cons_id'		=> 3362,
				'secret_key'	=> '1jY72ED457',
				'base_url' 		=> 'https://new-api.bpjs-kesehatan.go.id:8080',
				'service_name' 	=> ''
			);

			var $_confaplicare_tes	= array(
				'cons_id'		=> 3362,
				'secret_key'	=> '1jY72ED457',
				'base_url' 		=> 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' 	=> ''
			);

			var $_kodeppk = '1027R007';

			var $_profiling = 'No';

			var $_sections = array(
			        'config'  => FALSE,
			        'uri_string'  => FALSE,
			        'session_data'  => FALSE,
			        'query_toggle_count'  => TRUE,
			        'controller_info'  => FALSE,
			        'http_headers'  => FALSE,
			        'memory_usage'  => TRUE,
			        'post'  => TRUE,
			        'benchmarks'  => TRUE,
			        'queries' => TRUE,
			        'get' => TRUE
				);

			var $_kd_bangsal = array(
	            'ISO' => 'RUANG ISOLASI',
	            'KL1' => 'KELAS I',
	            'KL2' => 'KELAS II',
	            'KL3' => 'KELAS III',
	            'NIC' => 'NICU',
	            'HCU' => 'HCU',
	            'ICU' => 'ICU'
	        );
			
			var $_judul_tanya_jawab = array(
				1 => 'Bagaimana cara mendaftar menggunakan cara bayar umum?',
				2 => 'Bagaimana cara membatalkan booking pada cara bayar umum?',
				3 => 'Bagaimana cara untuk mengganti poliklinik dan dokter pada cara bayar umum?',
				4 => 'Bagaimana cara mendaftar menggunakan cara bayar bpjs/jamkesnas?',
				5 => 'Bagaimana cara mendaftar menggunakan cara bayar bpjs/jamkesnas rujukan dari rawat inap',
				6 => 'Bagaimana cara membatalkan booking pada cara bayar bpjs/jamkesnas?',
				7 => 'Bagaimana cara memunculkan gambar qrcode?',
				8 => 'Bagaimana cara saya melihat informasi dokter yang sedang tidak praktik?',
				9 => 'Bagaimana saya lihat apakah ada kamar tersedia?',
				10 => 'Apakah saya bisa melihat riwayat kunjungan saya?',
				11 => 'Apakah saya bisa melihat status kepesertaan saya di bpjs?',
				12 => 'Informasi Map RSUD KOTA DEPOk'
			);

			var $_jawaban = array(
				1 => '<p><h3>Cara Mendaftar</h3></p><p>
					1. Silahkan klik pada menu <b>Booking Umum</b><br>
					2. kemudian pilih menu <b>Booking Cara Bayar Umum</b><br>
					3. kemudian pilih <b>Tanggal Kunjungan</b> anda<br>
					4. Setelah itu klik tombol <b>Proses</b>, tunggu sebentar system akan mengarahkan anda pada formulir selanjutnya <br>
					5. Setelah muncul halaman selanjutnya, silahkan pilih nama poli yang ingin anda tuju<br>
					6. Kemudian pilih nama dokter yang ingin anda tuju dan tekan tombol simpan<br> 
					7. Setelah beres menekan tombol simpan tunggu sebentar, system sedang melakukan penyimpanan data, jika berhasil maka system akan memberikan respon data berhasil tersimpan.</p>  
					<p>Catatan : <br> 
					<ul>
						<ol>1. Ketika memilih tanggal kunjungan yang kurang dari tanggal saat ini, maka system akan memberikan informasi kesalahan tanggal.</ol>
						<ol>2. Ketika memilih tanggal kunjungan yang anda sudah terdaftar maka system akan memunculkan informasi bahawa anda sudah terdaftar pada tanggal hari itu.</ol>
						<ol>3. Ketika memilih tanggal kunjungan pada hari libur nasional maka system juga akan memberikan informasi.</ol>
						<ol>4. Ketika memilih dokter yang sudah tidak ada kuota maka system akan memberikan informasi dokter kuota sudah penuh.</ol>
						<ol>5. Ketika dokter sedang tidak praktik system juga akan memberikan informasi pesan.</ol>
					</ul></p>',
				2 => '<p><h3>Cara Pembatalan Kunjungan Pasien</h3></p>
					 <p>
					 1. Silahkan klik pada menu <b>Booking Umum</b><br>
					 2. Kemudian klik menu <b>Pembatalan Booking</b><br>
					 3. Pilih list tabel data kunjungan anda yang akan di batalkan, kemudian klik tombol <b>Batalkan</b>
					 </p>',
				3 => '<p><h3>Cara Mengganti Poli & Dokter</h3></p>
					<p>
					1. Silahkan klik pada menu <b>Booking Umum</b><br>
					2. Kemudian klik menu <b>Perubahan Poli Kunjungan</b><br>
					3. Pilih list tabel data kunjungan anda yang akan di rubah, kemudian klik tombol <b>Ganti Poli</b>
					4. Kemudian pilih poliklinik yang ingin di tuju <br>
					5. Pilih dokter yang ingin di tuju <br>
					6. Klik tombol Update.
					</p>',
				4 => '<p><h3>Cara Mendaftar BPJS</h3></p>
				<p>
					1. Silahkan klik pada menu <b>Booking Bpjs</b><br>
					2. kemudian pilih menu <b>Booking Cara Bayar BPJS/JKN</b><br>
					3. kemudian pilih <b>Tanggal Periksa.</b><br>
					4. Isi formulir <b>No Rujukan</b> yang diberikan oleh faskes 1<br>
					5. Untuk no surat skdp tidak perlu di isi jika pasien baru pertama kali mendaftar dengan no rujukan, dan isi jika anda sudah melakukan kunungan kedua kalinya. Biasanya no skdp ini akan diberikan oleh rumah sakit.
					6. Setelah itu klik tombol <b>Proses</b>, tunggu sebentar system akan mengarahkan anda pada formulir selanjutnya <br>
					7. Setelah muncul halaman selanjutnya, silahkan pilih nama poli yang ingin anda tuju<br>
					8. Kemudian pilih nama dokter yang ingin anda tuju dan tekan tombol simpan<br> 
					9. Setelah beres menekan tombol simpan tunggu sebentar, system sedang melakukan penyimpanan data, jika berhasil maka system akan memberikan respon data berhasil tersimpan.</p>  
					<p>Catatan : <br> 
					<ul>
						<ol>1. Ketika memilih tanggal kunjungan yang kurang dari tanggal saat ini, maka system akan memberikan informasi kesalahan tanggal.</ol>
						<ol>2. Ketika memilih tanggal kunjungan yang anda sudah terdaftar maka system akan memunculkan informasi bahawa anda sudah terdaftar pada tanggal hari itu.</ol>
						<ol>3. Ketika memilih tanggal kunjungan pada hari libur nasional maka system juga akan memberikan informasi.</ol>
						<ol>4. Ketika memilih dokter yang sudah tidak ada kuota maka system akan memberikan informasi dokter kuota sudah penuh.</ol>
						<ol>5. Ketika dokter sedang tidak praktik system juga akan memberikan informasi pesan.</ol>
					</ul></p>',
				5 => '<p><h3>Cara Mendaftar Dengan No Rujukan BPJS</h3></p>
				<p>
					1. Silahkan klik pada menu <b>Booking Bpjs</b><br>
					2. kemudian pilih menu <b>Booking Rujukan Dari Rawatinap</b><br>
					3. kemudian pilih <b>Tanggal Periksa.</b><br>
					4. Isi formulir <b>No Sep</b> yang diberikan oleh Rumah Sakit yang memberikan pelayanan.<br>
					5. Untuk no surat skdp tidak perlu di isi jika pasien baru pertama kali mendaftar dengan no rujukan, dan isi jika anda sudah melakukan kunungan kedua kalinya. Biasanya no skdp ini akan diberikan oleh rumah sakit.
					6. Setelah itu klik tombol <b>Proses</b>, tunggu sebentar system akan mengarahkan anda pada formulir selanjutnya <br>
					7. Setelah muncul halaman selanjutnya, silahkan pilih nama poli yang ingin anda tuju<br>
					8. Kemudian pilih nama dokter yang ingin anda tuju dan tekan tombol simpan<br> 
					9. Setelah beres menekan tombol simpan tunggu sebentar, system sedang melakukan penyimpanan data, jika berhasil maka system akan memberikan respon data berhasil tersimpan.</p>  
					<p>Catatan : <br> 
					<ul>
						<ol>1. Ketika memilih tanggal kunjungan yang kurang dari tanggal saat ini, maka system akan memberikan informasi kesalahan tanggal.</ol>
						<ol>2. Ketika memilih tanggal kunjungan yang anda sudah terdaftar maka system akan memunculkan informasi bahawa anda sudah terdaftar pada tanggal hari itu.</ol>
						<ol>3. Ketika memilih tanggal kunjungan pada hari libur nasional maka system juga akan memberikan informasi.</ol>
						<ol>4. Ketika memilih dokter yang sudah tidak ada kuota maka system akan memberikan informasi dokter kuota sudah penuh.</ol>
						<ol>5. Ketika dokter sedang tidak praktik system juga akan memberikan informasi pesan.</ol>
					</ul></p>',
				6 => '<p><h3>Cara Pembatalan Kunjungan Pasien BPJS</h3></p>
					 <p>
					 1. Silahkan klik pada menu <b>Booking Bpjs</b><br>
					 2. Kemudian klik menu <b>Pembatalan Booking</b><br>
					 3. Pilih list tabel data kunjungan anda yang akan di batalkan, kemudian klik tombol <b>Batalkan</b>
					 </p>',
				7 => '<p>Cara Memunculkan Gambar Qr-Code</p>
				<p>
				1. Arahkan kursor anda kebagian bawah, pada area fitur.<br>
				2. Klik pada menu Lihat <b>QR-CODE</b>
				</p>',
				8 => '<p>Melihat Jadwal Dokter Tidak Praktik</p>
				<p>
				1. Arahkan kursor anda kebagian bawah, pada area fitur.<br>
				2. Klik pada menu Lihat <b>Kalender Dokter Tidak Praktik.</b>	
				</p>',
				9 => '<p>Melihat Ketersediaan Kamar</p><p>
				1. Klik pada menu <b>Informasi Kamar</b>
				2. Kemudian klik pada tombol <b>Lihat Detail</b>
				</p>',
				10 => '<p>Melihat Riwayat Kunjungan</p><p>
				1. Arahkan kursor anda kebagian bawah, pada area fitur.<br>
				2. Klik pada menu <b>Riwayat Kunjungan</b><br>
				3. Buka Tab <b>Riwayat Booking</b> untuk melihat jumlah booking<br>
				4. Klik pada kolom aksi tombol icon printer untuk mencetak bukti booking.<br>
				5. Klik pada tab <b>Riwayat Pemeriksaan</b> untuk melihat data pemeriksaan.<br>
				6. klik pada kolom tabel untuk melihat data-data pemeriksaan.
				</p>',
				11 => '<p>Cara Melihat Status Kepesetaan dan Info Faskes</p><p>
				1. Arahkan kursor anda kebagian bawah, pada area fitur.<br>
				2. Kemudian klik pada tombol <b>Apps Bpjs</b>.<br>
				3. Klik pada tab <b>Cari Lokasi Faskes</b>.<br>
				4. Ketikan nama daerah yang ingin anda cari faskes-nya pada form input kemudian pilih tipe faskes yang anda cari, kemudian tekan tombol proses.
				5. Klik pada tab <b>Lihat Informasi Pelayanan</b>.<br>
				6. Masukan no kartu bpjs kemudian pilih tanggal awal dan tanggal akhir, kemudian tekan tombol proses.<br>
				5. Klik pada tab <b>Cek Status Kepesetaan</b>.<br>
				6. Masukan No Induk KTP kemudian pilih tanggal pelayanan, kemudian tekan tombol proses.
				</p>',
				12 => '<p>Cara Melihat Informasi Map</p><p>
				1. Arahkan kursor anda kebagian bawah, pada area fitur.<br>
				2. Klik pada menu <b>Maps Rsud Kota Depok.</b>
				</p>'
			);

		}