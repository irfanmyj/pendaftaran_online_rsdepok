<?php get_template('inc/navbar-page.html'); ?>
	<!-- map -->
	<div class="map-page segments-page">
		<div class="container-fluid">
			<div class="content-map">
                <?php echo $maps; ?>
            </div>
		</div>
	</div>
	<!-- end map -->
<?php get_template('inc/footer.html'); ?>
<?php get_template('inc/endhtml.html'); ?>