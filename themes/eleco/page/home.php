<?php  
	get_template('inc/navbar.html');
	get_template('inc/sidebar-header.html');
	get_template('inc/slide.html');
	get_template('inc/filter-home.html');
	get_template('inc/features.html');
	get_template('inc/footer.html');
?>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi</h5>
	            </div>
	            <div class="modal-body" id="msg">
	            	<h1>Segera akan hadir...</h1>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<div class="content" style="display: none;" id="modalNotifikasi">
	<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header bg-primary">
	                <h5 class="modal-title text-white">Informasi</h5>
	            </div>
	            <div class="modal-body" id="msg">
	            	<?php if(!empty($res)){ ?>
						<?php foreach ($res as $k => $v) { ?>
						<div class="container">
							<div class="single-post">
								<!-- <img src="images/post1.jpg" alt=""> -->
								<a href=""><h5><?php echo ($v->judul) ? $v->judul : ''; ?></h5></a>
								<p class="date"><?php echo ($v->masuk_tgl) ? '<i class="fa fa-clock-o"></i>'. tanggal_indo($v->masuk_tgl,true) : ''; ?></p>
								<p class="title">
									<?php echo (@$v->tgl_awal) ? 'Tanggal Mulai Cuti : '. tanggal_indo($v->tgl_awal,true) : ''; ?><br> 
									<?php echo (@$v->tgl_akhir) ? 'Tanggal Akhir Cuti : '. tanggal_indo($v->tgl_akhir,true) : ''; ?>
								</p>
								<?php echo ($v->nm_dokter) ? $v->nm_dokter .' - '. $v->nm_poli : '';?>
								<br>				
								<?php echo ($v->keterangan) ? $v->keterangan : '';?>				
							</div>
							<?php if($v->judul) { ?>
							<div class="author">
								<div class="content-text">
									<h5>Admin RSUD KOTA DEPOK</h5>
								</div>
							</div>
							<?php }else{ echo ''; } ?>
						</div>
						<?php } ?>
					<?php }else{ echo 'Data tidak tersedia.'; } ?>
	            </div>
	            <div class="modal-footer bg-primary">
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var notifikasi = "<?php echo notifikasi('jumlah');?>";
		$('#artikel').click(function(){
			$('#modalUmum').css('display','block');
			$('#exampleModal2').modal();
		});

		if(notifikasi>0)
		{
			$('#modalNotifikasi').css('display','block');
			$('#exampleModal3').modal();
		}
	});
</script>
<?php
	get_template('inc/endhtml.html');
?>
