<?php get_template('inc/navbar-page.html'); ?>
<div class="open-hours segments-page">
    <div class="container">
        <div class="content b-shadow">
            <div class="title" style="margin-bottom: 0px;">
                <h5><?php echo strtoupper($title); ?> | <?php echo strtoupper(tanggal_indo(date('Y-m-d')));?></h5>
            </div>

            <div class="content no-mb">
                <form method="post" action="<?php echo base_url('info/search_info_kuota');?>">
                    <div class="row p-2">
                        <div class="col-1">&nbsp;</div>
                        <div class="col-4 pull-left mb-2">
                            <input type="date" name="tanggal_periksa" class="form-control" value="<?php echo date('Y-m-d'); ?>" id="search_tanggal">
                        </div>
                        <div class="col-4 pull-left mb-2">
                            <?php 
                            echo htmlSelectFromArray($poli, 'name="kd_poli" id="search_kd_poli" style="width:100%;" class="form-control search2"', true);
                            ?>
                        </div>

                        <div class="col-2 mb-2">
                            <button type="submit" class="btn btn-primary col-12">Cari</button>
                        </div>
                        <div class="col-1">&nbsp;</div>
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table_info_kuota">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th>Tanggal</th>
                                            <th>Hari</th>
                                            <th>Nama Poli</th>
                                            <th>Nama Dokter</th>
                                            <th style="text-align:center;">Kuota</th>
                                            <th style="text-align:center;">Tersedia</th>
                                            <th style="text-align:center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list_daftar_kuota"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="content" style="display: none;" id="modalUmum">
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Pendaftaran Online</h5>
                    <button class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>
                </div>
                <div class="modal-body" id="show_data">
                    <div class="form-group">
                        <select name="kd_pj" class="form-control">
                            <option>-------</option>
                            <option value="UMU">Umum</option>
                            <option value="JKN">JKN</option>
                        </select>
                    </div>

                    <div id="jkn" style="display: none;">
                        <div class="form-group" id="select_asal_rujukan">
                            <h5>Asal Rujukan</h5>
                            <select name="asal_rujukan" class="form-control" id="asal_rujukan">
                                <option value="---">---</option>
                                <option value="faskes1">1. Faskes 1 (PKM/KLINIK PRATAMA)</option>
                                <option value="faskes2">2. Faskes 2 (RS)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <h5>No Rujukan</h5>
                            <input type="text" name="no_rujukan" id="no_rujukan" placeholder="Masukan no rujukan anda" class="form-control">
                        </div>

                        <div class="form-group" style="display: none;" id="form_no_skdp"> 
                            <h5>No SKDP</h5>
                            <input type="text" name="no_skdp" id="no_skdp" placeholder="Masukan No SKDP" class="form-control">
                        </div>
                    </div>
                    <div class="content no-mb" id="spiner" style="display: none;">
                        <div class="form-group">
                            <button class="btn btn-primary" type="button" disabled>
                              <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                              Silahkan tunggu, data sedang di proses...
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="tombol1" style="display: none;"><i class="fa fa-save"></i> Daftar Sekarang</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="close" id="tombol2" style="display: none;"> Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
   $(document).ready(function(){
        $('#search_kd_poli').select2();
        var link = '<?php echo base_url('info');?>';

        $('#search_info_kuota').click(function(){
            var tanggal = $('#search_tanggal').val();
            var kd_poli = $('#search_kd_poli').val();
            $.ajax({
                type : 'post',
                url : link+'/search_info_kuota',
                data : 'tanggal_periksa='+tanggal+'&kd_poli='+kd_poli,
                success : function(res){
                    $('#list_daftar_kuota').html(res);
                }
            });
        });

        // Code ID : daftar_langsung
        $('#list_daftar_kuota').on('click','#daftar_langsung',function(){
            var kd_poli = $(this).data('kd_poli');
            var kd_dokter = $(this).data('kd_dokter');
            var tanggal_periksa = $(this).data('tanggal_periksa');
            
            $('#modalUmum').css('display','block');
            $('#exampleModal2').modal();
            check_daftar(tanggal_periksa);

            // Area show select JKN
            $('[name="kd_pj"]').change(function(){
                var kd_pj = $(this).val();

                if(kd_pj=='UMU'){
                    $('#jkn').css('display','none');
                    $('#tombol1').css('display','block');
                    $('#tombol1').click(function(){
                        $.ajax({
                            type : 'post',
                            url : link+'/save_umum',
                            data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&kd_pj='+kd_pj+'&tanggal_periksa='+tanggal_periksa,
                            success : function(res)
                            {
                                var dt = JSON.parse(res);
                                $('#show_data').html(dt.msg);
                                $('#tombol1').css('display','none');
                                $('#tombol2').css('display','block');
                            }
                        });
                    });
                }else if(kd_pj=='JKN'){
                    $('#jkn').css('display','block');
                    $('#tombol1').css('display','block');
                    $('#tombol2').css('display','none');

                    // No Rujukan
                    $('[name="asal_rujukan"]').change(function(){
                        var asal_rujukan = $(this).val();
                        if(asal_rujukan=='faskes1')
                        {
                            var func = 'getNoRujukan';
                        }
                        else
                        {
                            var func = 'getNoRujukan1';
                        }

                        $('[name="no_rujukan"]').change(function(){
                            var no_rujukan = $(this).val();
                            $('#form_no_skdp').css('display','block');
                            $('#form_no_skdp').change(function(){
                                var no_skdp = $('#form_no_skdp [name="no_skdp"]').val();
                                var noskdp = $('#form_no_skdp [name="no_skdp"]').val().length;
                                if(noskdp < 6)
                                {
                                    alert('Nomor skdp tidak boleh lebih dari enam (6) digit.');
                                }
                                else
                                {
                                    $.ajax({
                                        type : 'post',
                                        url : '<?php echo base_url('Get_ajax');?>/'+func,
                                        data : 'no_rujukan='+no_rujukan+'&tgl_registrasi='+tanggal_periksa+'&no_skdp='+no_skdp,
                                        success : function(res)
                                        {
                                            var JsDt = JSON.parse(res);
                                            if(JsDt.msg=='')
                                            {   
                                                $('#tombol1').click(function(){
                                                    $.ajax({
                                                        type : 'post',
                                                        url : link+'/save_bpjs',
                                                        data : 'tanggal_periksa='+tanggal_periksa+'&kd_poli='+kd_poli+'&kd_dokter='+kd_dokter+'&kd_pj='+kd_pj+'&no_rujukan='+no_rujukan+'&no_skdp='+no_skdp,
                                                        success : function(res)
                                                        {
                                                            var dt = JSON.parse(res);
                                                            $('#show_data').html(dt.msg);
                                                            $('#tombol1').css('display','none');
                                                            $('#tombol2').css('display','block');
                                                        }
                                                    });
                                                });
                                            }else{
                                                alert(JsDt.msg);
                                            }
                                        }
                                    });
                                }
                            });
                        });
                    });
                    
                }else{
                    $('#jkn').css('display','none');
                    $('#tombol1').css('display','none');
                    $('#tombol2').css('display','none');
                }
            });
        });
        load();
    }); 

    function load() {
        setTimeout(function () {
            $.ajax({
                url : "<?php echo base_url('info/get_info_kuota');?>",
                type: 'get',
                success: function (res) {
                    $('#list_daftar_kuota').html(res);
                },
                complete: load
            });
        }, 1000);
    }

    function check_daftar(tanggal_periksa='')
    {
        $.ajax({
            type : 'post',
            url : '<?php echo base_url('info/checking_reg');?>',
            data : 'tanggal_periksa='+tanggal_periksa,
            success : function(res)
            {
                var dt = JSON.parse(res);
                if(dt.sts !='')
                {
                    $('#show_data').html(dt.msg);
                    $('#tombol1').css('display','none');
                    $('#tombol2').css('display','block');   
                }
            }
        });
    }
</script>
<?php get_template('inc/endhtml.html'); ?>