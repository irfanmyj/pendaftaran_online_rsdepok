	<?php get_template('inc/navbar-page.html'); ?>
	<!-- features -->
	<div class="features segments-page">
		<div class="container-pd">

			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bpjslansia/booking');?>">
					<div class="content b-shadow">
						<span><img src="<?php echo base_url('vendor/eleco/images/bpjs.png');?>" width="38"></span>
						<h4>Booking Cara Bayar JKN/BPJS</h4>
						<p>Silahkan klik disini untuk booking dengan cara pembayaran JKN/BPJS.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bpjslansia/rawatinap');?>">
					<div class="content b-shadow">
						<span><img src="<?php echo base_url('vendor/eleco/images/bpjs.png');?>" width="38"></span>
						<h4>BOOKING RUJUKAN DARI RAWAT INAP</h4>
						<p>Silahkan klik disini untuk booking dengan cara pembayaran JKN/BPJS.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bpjslansia/batal');?>">
					<div class="content b-shadow">
						<i class="fa fa-times-circle"></i>
						<h4>Pembatalan Booking</h4>
						<p>Silahkan klik disini untuk melakukan pembatalan booking.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="text">
					<p>
						Catatan : Untuk perubahan poli, akan berhasil jika poli yang dituju masih tersedia kuota pendaftaranya.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end features -->
	<?php get_template('inc/footer.html'); ?>
	<?php get_template('inc/endhtml.html'); ?>