<?php get_template('inc/navbar-page.html'); ?>
<!-- Booking Umum -->
<div class="form-element segments-page">
	<div class="container">

		<div class="content no-mb">
			<div class="form-group">
				<h5>Tanggal Periksa</h5>
				<input type="date" class="form-control" value="" name="tgl_registrasi" id="tgl_registrasi">
			</div>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<h5>Asal Rujukan</h5>
				<select name="asal_rujukan" class="form-control asal_rujukan">
					<option value="---">---</option>
					<option value="faskes1">1. Faskes 1 (PKM/KLINIK PRATAMA)</option>
					<option value="faskes2">2. Faskes 2 (RS)</option>
				</select>
			</div>
		</div>

		<div class="content no-mb" id="rujukan" style="display: none;">
			<div class="form-group">
				<h5>No Rujukan</h5>
				<input type="text" class="form-control" value="" name="no_rujukan" id="no_rujukan">
			</div>
		</div>

		<div class="content no-mb" id="no_skdp1" style="display: none;">
			<div class="form-group">
				<h5>No Surat SKDP</h5>
				<input type="number" max="6" class="form-control" value="" name="no_skdp" id="no_skdp" placeholder="Jika tidak ada no SKDP, silahkan kosongkan.">
				<span class="text">Formulir ini hanya bisa diisi dengan angka.</span>
			</div>
		</div>

		<div class="content no-mb" id="spiner" style="display: none;">
			<div class="form-group">
				<button class="btn btn-primary" type="button" disabled>
				  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
				  Silahkan tunggu, data sedang di proses...
				</button>
			</div>
		</div>

		<div class="content no-mb">
			<button type="submit" class="btn btn-primary" id="tombol1"><i class="fa fa-gear"></i> Proses</button>
		</div>
	</div>
</div>

<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi Gagal</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body" id="msg"></div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- end Booking Umum -->
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('Get_ajax');?>';
		$('#tgl_registrasi').change(function(){
			var tgl_registrasi = $(this).val();
			if(tgl_registrasi)
			{
				$.ajax({
					type : 'post',
					url : link+'/umum_step1',
					data : 'tgl_registrasi='+tgl_registrasi,
					success : function(res)
					{
						var JsDt = JSON.parse(res);
						if(JsDt.msg == '')
						{
							$('#rujukan').css('display','block');	
							$('#no_skdp1').css('display','block');	
						}
						else
						{
							$('#rujukan').css('display','none');	
							$('#no_skdp1').css('display','none');	
							$('#modalUmum').css('display','block');
							$('#msg').html(JsDt.msg);
							$('#exampleModal2').modal();
						}
					}
				});
			}
		});

		$('#tombol1').click(function(){
			//$('#spiner').css('display','block');
			var tgl_registrasi = $('#tgl_registrasi').val();
			var no_rujukan = $('#no_rujukan').val();
			var no_skdp = $('#no_skdp').val();
			var noskdp = $('#no_skdp').val().length;
			var asal_rujukan = $('.asal_rujukan').val();
			
			if(asal_rujukan=='---')
			{
				alert('Asal Rujukan Wajib dipilih.');
			}
			else
			{
				if(asal_rujukan=='faskes1')
				{
					var func = 'getNoRujukan';
				}
				else
				{
					var func = 'getNoRujukan1';
				}

				if(noskdp > 6)
				{
					alert('Nomor skdp tidak boleh lebih dari enam (6) digit.');
				}
				else
				{
					$.ajax({
						type : 'post',
						url : link+'/'+func,
						data : 'no_rujukan='+no_rujukan+'&tgl_registrasi='+tgl_registrasi+'&no_skdp='+no_skdp,
						success : function(res)
						{
							var JsDt = JSON.parse(res);
							if(JsDt.msg == '')
							{

								window.location = '<?php echo base_url('bpjs/bpjs2');?>';
							}
							else
							{
								$('#spiner').css('display','none');
								$('#modalUmum').css('display','block');
								$('#msg').html(JsDt.msg);
								$('#exampleModal2').modal();
							}	
						}
					});
				}
			}
			
		});

		$('#no_skdp').change(function(){
			var val = $(this).val().length;
			if(val > 6)
			{
				$('#no_skdp').css('background','red');
				$('#no_skdp').css('color','white');
				alert('Nomor skdp tidak boleh lebih dari enam (6) digit.'+val);
			}
		});
	});
</script>
<?php get_template('inc/endhtml.html'); ?>