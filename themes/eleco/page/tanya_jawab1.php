<?php get_template('inc/navbar-page.html'); ?>
	<!-- accordion -->
	<div class="open-hours segments-page">
		<div class="container">
			<div class="content b-shadow">
				<div class="title" style="margin-bottom: 0px;">
					<h5><?php echo strtoupper($title); ?> | <?php echo strtoupper(tanggal_indo(date('Y-m-d')));?></h5>
				</div>
				<div class="content no-mb">
					<div id="accordionPage1" class="accordion b-shadow">
						<?php
						$no = 0; 
						foreach ($judul as $k => $v) { 
						$no++;
						?>
							<div class="card">
								<div id="header<?php echo $no;?>" class="card-header" data-toggle="collapse" data-target="#collapse<?php echo $no;?>">
									<h6> <?php echo $no .'. '. $v;?></h6>
								</div>
								<div id="collapse<?php echo $no;?>" class="collapse" aria-labelledby="header<?php echo $no;?>" data-parent="#accordionPage1">
									<div class="card-body">
										<span><?php echo $isi[$k]; ?></span>
									</div>
								</div>
							</div>
						<?php } ?>
						
					</div>
				</div>
				<div id="showtext" style="display: none;">
					
				</div>
			</div>
		</div>
	</div>
	<!-- end accordion -->
	<?php get_template('inc/footer.html'); ?>
	
	<?php get_template('inc/endhtml.html'); ?>