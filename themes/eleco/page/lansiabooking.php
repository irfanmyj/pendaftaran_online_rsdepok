	<?php get_template('inc/navbar-page.html'); ?>
	<!-- features -->
	<div class="features segments-page">
		<div class="container-pd">
			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bookinglansia/umum');?>">
					<div class="content b-shadow">
						<i class="fa fa-money"></i>
						<h4>Booking Cara Bayar Umum</h4>
						<p>Silahkan klik disini untuk booking dengan cara pembayaran umum.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bookinglansia/batal');?>">
					<div class="content b-shadow">
						<i class="fa fa-times-circle"></i>
						<h4>Pembatalan Booking</h4>
						<p>Silahkan klik disini untuk melakukan pembatalan booking.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="col-12 px-1">
					<a href="<?php echo base_url('bookinglansia/gantipoli');?>">
					<div class="content b-shadow">
						<i class="fa fa-pencil-square"></i>
						<h4>Perubahan Poli Kunjungan</h4>
						<p>Silahkan klik disini untuk melakukan perubahan poli.</p>
					</div>
					</a>
				</div>
			</div>

			<div class="row">
				<div class="text">
					<p>
						Catatan : Untuk perubahan poli, akan berhasil jika poli yang dituju masih tersedia kuota pendaftaranya.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end features -->
	<?php get_template('inc/footer.html'); ?>
	<?php get_template('inc/endhtml.html'); ?>