<?php get_template('inc/navbar-page.html'); ?>
<!-- Booking Umum -->
<div class="form-element segments-page">
	<div class="container">

		<div class="content no-mb">
			<table class="table table-responsive table-striped">
				<tr>
					<td>Nomor Rekam Medis</td>
					<td width="10">:</td>
					<td><?php echo $this->session->userdata('no_rkm_medis'); ?></td>
				</tr>
				<tr>
					<td>Nomor Rujukan</td>
					<td width="10">:</td>
					<td><?php echo $this->session->userdata('no_rujukan'); ?></td>
				</tr>
				<tr>
					<td>Nama Pasien</td>
					<td width="10">:</td>
					<td><?php echo $this->session->userdata('nm_lengkap'); ?></td>
				</tr>
			</table>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<h5>Poliklinik Tujuan</h5>
				<?php
                echo htmlSelectFromArray($poli, 'name="kd_poli" id="kd_poli" style="width:100%;" class="form-control search2"', true);
                ?>
			</div>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<h5>Dokter Tujuan</h5>
				<div id="id_dokter">
				<?php
                echo htmlSelectFromArray($dokter, 'name="kd_dokter" id="kd_dokter" style="width:100%;" class="form-control"', true);
                ?>
				</div>
			</div>
		</div>

		<div class="content no-mb">
			<div class="form-group">
				<div class="text">
					<h3 id="info_kuota"></h3>
				</div>
			</div>
		</div>

		<div class="content no-mb" style="display: none;" id="spiner">
			<button class="btn btn-primary" type="button" disabled>
			  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
			  Silahkan tunggu, data sedang di proses...
			</button>
		</div>

		<div class="content no-mb">
			<button type="submit" class="btn btn-primary" id="tombol2"><i class="fa fa-gear"></i> Simpan</button>
			<a href="<?php echo base_url('bpjs/booking'); ?>" class="btn btn-primary" style="display: none;" id="kembali"><i class="fa fa-gear"></i> Kembali</a>
		</div>
	</div>
</div>
<div class="content" style="display: none;" id="modalUmum">
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Informasi</h5>
	                <button class="close" data-dismiss="modal" aria-label="close">
	                    <span aria-hidden="true"><i class="fa fa-close"></i></span>
	                </button>
	            </div>
	            <div class="modal-body" id="msg"></div>
	            <div class="modal-footer">
	                <button type="button" id="tutups" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
	                <a href="<?php echo base_url('home/view');?>" type="button" id="sukses" class="btn btn-secondary" style="display: none;">Kembali</a>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<!-- end Booking Umum -->
<?php get_template('inc/footer.html'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		var link = '<?php echo base_url('Get_ajax');?>';
		var hari = '<?php echo $hari;?>';
		$('.search2').select2();

		$('#kd_poli').change(function(){
			var kd_poli = $(this).val();
			$.ajax({
				type : 'post',
				url : link+'/getDokter',
				data : 'kd_poli='+kd_poli+'&hari='+hari,
				success: function(res)
				{
					$('#id_dokter').html(res);

					$('#kd_dokter').change(function(){
					var kd_dokter = $(this).val();
					var kd_poli = $('#kd_poli').val();

					if(kd_dokter!='')
					{
						$.ajax({
							type : 'post',
							url : link+'/umum_step2',
							data : 'kd_dokter='+kd_dokter+'&kd_poli='+kd_poli,
							success : function(res){
								var JsDt = JSON.parse(res);
								if(JsDt.sts==1)
								{
									$('#modalUmum').css('display','block');
									$('#info_kuota').css('display','none');
									$('#tombol2').css('display','none');
									$('#kembali').css('display','block');
									$('#msg').html(JsDt.msg);
									$('#exampleModal2').modal();
								}
								else
								{
									$('#tombol2').css('display','block');
									$('#kembali').css('display','none');
									$('#info_kuota').css('display','block');
									$('#info_kuota').html(JsDt.msg);
								}
							}
						});
					}
					else
					{
						alert('Silahkan pilih nama dokter yang ingin anda tuju.');
					}
				});
				}
			});
		});

		$('#tombol2').click(function(){
			var kd_dokter = $('#kd_dokter').val();
			var kd_poli = $('#kd_poli').val();
			if(kd_poli=='' && kd_dokter=='')
			{
				alert('Silahkan poli Poliklinik dan Dokter');
			}
			else
			{
				$('#spiner').css('display','block');
				$.ajax({
					type : 'post',
					url : link+'/bpjs_step3',
					data : 'kd_poli='+kd_poli+'&kd_dokter='+kd_dokter,
					success : function(res)
					{
						var JsDt2 = JSON.parse(res);
						if(JsDt2.sts == 1)
						{
							$('#modalUmum').css('display','block');
							$('#msg').html(JsDt2.msg);
							$('#exampleModal2').modal();
							$('#spiner').css('display','none');
						}else{
							$('#spiner').css('display','none');
							$('#modalUmum').css('display','block');
							$('#sukses').css('display','block');
							$('#tutups').css('display','none');
							$('#msg').html(JsDt2.msg);
							$('#exampleModal2').modal();
						}
					}
				});
			}
		});
	});
</script>
<?php get_template('inc/endhtml.html'); ?>