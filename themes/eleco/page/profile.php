<?php get_template('inc/navbar-page.html'); ?>
	<!-- profile -->
	<div class="container">
		<div class="profile segments-page">
			<div class="profile-banner">
				<div class="content">
					<img src="images/testimonial1.png" alt="">
					<h4><?php echo $this->session->userdata('nm_lengkap'); ?></h4>
					<h4><?php echo $this->session->userdata('no_rkm_medis'); ?></h4>
					<span><?php echo $this->session->userdata('alamat'); ?></span><br><br>
					<a href="<?php echo base_url('info/viewprofile/'.$this->session->userdata('no_rkm_medis'));?>" class="button button-white"><i class="fa fa-user"></i>Lihat Profile Detail</a>
				</div>
			</div>
			<div class="content-info b-shadow">
				<div class="row">
					<div class="col-4">
						<ul>
							<li><?php echo $jkp; ?></li>
							<li>Kunjungan Poli</li>
						</ul>
					</div>
					<div class="col-4">
						<ul>
							<li><?php echo $jkr; ?></li>
							<li>Kunjungan Ranap</li>
						</ul>
					</div>
					<div class="col-4">
						<ul>
							<li><?php echo $jkl; ?></li>
							<li>Jumlah Login</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="p-gallery">
				<div class="container" style="background: #ffffff;">
					<div class="row">
						<div class="col-4 px-1">
							<div class="wrap-content b-shadow">
								<table class="table table-striped table-responsive display" style="width:100%" id="example">
									<thead>
										<tr>
											<th>No</th>
											<th>Aktivitas</th>
											<th>Tanggal</th>
										</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach ($r as $k => $v) { ?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $v->activity; ?></td>
											<td><?php echo tanggal_indo($v->activity_date,true); ?></td>
										</tr>
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-8 px-2" id="kunjungan" style="height: 300px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end profile -->
	<?php get_template('inc/footer.html'); ?>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script type="text/javascript">
		 Highcharts.chart('kunjungan', {
		    chart: {
			    type: 'column'
			},
            exporting: {
                enabled: false
            },
		    title: {
			    text: 'Grafik Kunjungan'
			},
			subtitle: {
				text: <?=json_encode('Tanggal : '.date("d m Y"));?>
			},
		    xAxis: {
		        categories: <?=json_encode($jmlp);?> ,
				title: {
				    enabled: false
				}
			},
			yAxis: {
				title: {
					text: 'Jumlah Pasien'
				},
				labels: {
					formatter: function () {
						return this.value;
					}
				}
			},
			tooltip: {
				split: true,
				valueSuffix: ''
			},
			plotOptions: {
				area: {
				stacking: 'normal',
				lineColor: '#666666',
				lineWidth: 1,
			    	marker: {
						lineWidth: 1,
						lineColor: '#666666'
					}
				}
			},
			series: [{
				name: 'Poliklinik dan Rawat Jalan <br>  Total Keseluruhan : <?= $jmlt;?>',
				data: <?=json_encode($jmlk);?>
			}]
		});
	</script>
	<?php get_template('inc/endhtml.html'); ?>