<!-- ############ PAGE START-->
<!-- only need a height for layout 4 -->
<div class="padding">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
	    		<div class="col-md-4 col-sm-12 col-xs-12">
                    <button class="btn btn-xs white" onclick="formAdd()"><i class="fa fa-plus"></i> Tambah Data</button> &nbsp;
                    <button class="btn btn-xs white" disabled="true" id="btnDelete" onclick="formDelete()"><i class="fa fa-trash-o"></i> Hapus Data</button>
                </div>
                <!-- <div class="col-md-2 col-sm-12 col-xs-12">
                    
                </div> -->
                <div class="col-md-5 col-sm-12 col-xs-12">

                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <input type="text" class="form-control input-sm" name="cari" placeholder="Cari" value="<?php echo ($f!='f') ? $f : ''; ?>" onkeyup="event.keyCode == 13 ? window.location = '<?php echo base_url($this->uri->segment(1).'/view/'); ?>' + this.value : '';">
                </div>
	    	</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px; ">
			<div class="box">
			    <div class="table-responsive">
			      <table class="table table-hover table-bordered table-striped b-t">
			        <thead>
			          <tr>
			            <th style="width:20px;"></th>
                        <th>Nip</th>
                        <th>Nama Lengkap</th>
                        <th>Tgl Lahir</th>
                        <th>Tempat Lahir</th>
                        <th>Jk</th>
                        <th>Telp</th>
                        <th>Status</th>
			          </tr>
			        </thead>
			        <tbody>
			        	<?php 
	                		//print_r($_SESSION);
	                		foreach ($r as $idrw => $rw) {
	                            echo '<tr style="cursor:pointer" ' .showValRec($rw) . '>';
	                            echo '<td class="text-center chk"><input type="checkbox" name="id[]" value="' . $rw->id_pegawai . '"></td>';
                                echo '<td class="tr_mod">'.$rw->nip.'</td>';
                                echo '<td class="tr_mod">'.$rw->nm_lengkap.'</td>';
                                echo '<td class="tr_mod">'.$rw->tgl_lahir.'</td>';
                                echo '<td class="tr_mod">'.$rw->tmpt_lahir.'</td>';
                                echo '<td class="tr_mod">'.$jk[$rw->jk].'</td>';
                                echo '<td class="tr_mod">'.$rw->telp.'</td>';
                                echo '<td class="tr_mod">'.$status[$rw->id_status].'</td>';
	                            echo '</tr>';
	                        }

	                        if (count($r) == 0) {
	                            echo '<tr><td colspan="100%" style="text-align:center">There\'s no login log</td></tr>';
	                        }
	                	?>
			        </tbody>
			      </table>
			    </div>
			  </div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<?php  
            	$p = $p ? $p + 1 : 1;

                $paging = array();
                for ($i = 1; $i <= ceil($jum / $l); $i++) {
                    $paging[$i] = $i;
                }
            	?>

            	<div class="col-md-3 text-left">
                    <?php echo 'Limit : ' . htmlSelectFromArray($limit, 'name="l" style="width:100px;" onchange="window.location=\'' . base_url($this->uri->segment(1).'/view/') . $f . '/\'+this.value"/0', true, $l); ?>
                </div>
                <div class="col-md-7 text-right">
                    halaman : <?php echo htmlSelectFromArray($paging, 'name="p" style="width:100px" onchange="window.location=\'' . base_url($this->uri->segment(1).'/view/') . $f . '/' . $l . '/\'+this.value"', false, $p); ?>
                </div>
                <div class="col-md-2 text-center">
                    Total Data : <?php echo $jum; ?>
                </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade animate" id="modalMod" style="display: none">
    <div class="modal-dialog modal-lg fade-left">
        <div class="modal-content">
            <div class="modal-header blue">
                <h4 class="modal-title">Manajemen Kepegawaian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url($this->uri->segment(1).'/save') ?>" method="post" id="modForm" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="id">
                    <input type="hidden" name="old_image">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Tanggal Masuk</label>
                                        <div class="col-sm-12">
                                            <input type="date" class="form-control input-sm" name="masuk_tgl" placeholder="" required="true">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Tanggal Masuk</label>
                                        <div class="col-sm-12">
                                            <input type="date" class="form-control input-sm" name="masuk_tgl" placeholder="" required="true">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Nip</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm nip" name="nip" placeholder="Nomor Induk Pegawai" required="true">
                                            <span class="text-center txt_nip"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Nama Lengkap</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm" name="nm_lengkap" placeholder="Nama Lengkap" required="true">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Tanggal Lahir</label>
                                        <div class="col-sm-12">
                                            <input type="date" class="form-control input-sm" name="tgl_lahir">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Tempat Lahir</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm" name="tmpt_lahir" placeholder="Tempat Lahir" required="true">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Telp</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm" name="telp" placeholder="Telp">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Jenis Kelamin</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($jk, 'name="jk" id="jk" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Golongan Darah</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($goldarah, 'name="gol_darah" id="gol_darah" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Provinsi</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($provinsi, 'name="provinsi" id="provinsi" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Kabupaten</label>
                                        <div class="col-sm-12" id="kabupaten"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Kecamatan</label>
                                        <div class="col-sm-12" id="kecamatan"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-3 control-label">Desa</label>
                                        <div class="col-sm-12" id="desa"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Departement</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectGroup($departement,'id_departement');
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Golongan</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($golongan, 'name="id_golongan" id="id_golongan" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Jabatan</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($jabatan, 'name="id_jabatan" id="id_golongan" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Email</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control input-sm email" name="email" placeholder="Email">
                                            <span class="text-center txt_email"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Password</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control input-sm" name="password" placeholder="Password">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Level Akses</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($level, 'name="id_level" id="id_level" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Foto</label>
                                        <div class="col-sm-12">
                                        <input type="file" name="image" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tun" class="col-sm-12 control-label">Status</label>
                                        <div class="col-sm-12">
                                        <?php
                                        echo htmlSelectFromArray($status, 'name="id_status" id="id_status" style="width:100%;" class="form-control select2"', true);
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="modForm" class="btn btn-sm white"><i class="fa fa-arrow-circle-right"></i> Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- ############ PAGE END-->
<?php get_template('inc/js.html');?>

<script type="text/javascript">
    var link = '<?php echo base_url('get_ajax');?>';
    if (!checkPrivilege('<?php echo hak_akses(a('uri'),a());?>')) {
        history.go(-1);
    }

	$(document).ready(function(){
		 $('.tr_mod').click(function(){
            if(checkPrivilege('<?php echo hak_akses('pegawai_edit','edit');?>'))
            {
                var tr = $(this).parent();
                $('#modalMod :input').val('');
                $('#modalMod [name="id"]').val(tr.data('id_pegawai'));
                $('#modalMod [name="nip"]').val(tr.data('nip'));
                $('#modalMod [name="email"]').val(tr.data('email'));
                $('#modalMod [name="telp"]').val(tr.data('telp'));
                $('#modalMod [name="nm_lengkap"]').val(tr.data('nm_lengkap'));
                $('#modalMod [name="masuk_tgl"]').val(tr.data('masuk_tgl'));
                $('#modalMod [name="tgl_lahir"]').val(tr.data('tgl_lahir'));
                $('#modalMod [name="tmpt_lahir"]').val(tr.data('tmpt_lahir'));
                $('#modalMod [name="id_departement"]').select2('val',tr.data('id_departement'));
                $('#modalMod [name="jk"]').select2('val',tr.data('jk'));
                $('#modalMod [name="id_golongan"]').select2('val',tr.data('id_golongan'));
                $('#modalMod [name="id_status"]').select2('val',tr.data('id_status'));
                $('#modalMod [name="gol_darah"]').select2('val',tr.data('gol_darah'));
                $('#modalMod [name="id_jabatan"]').select2('val',tr.data('id_jabatan'));
                $('#modalMod [name="id_level"]').select2('val','<?php echo $this->session->userdata('id_level');?>');
                $('#modalMod [name="old_image"]').val(tr.data('foto'));

                if(tr.data('provinsi'))
                {
                    $('#modalMod [name="provinsi"]').select2('val',tr.data('provinsi'));
                    var idpv = tr.data('provinsi');
                    $.ajax({
                        type  : 'post',
                        data  : 'id_prov='+idpv,
                        url   : link+'/getkab',
                        success : function(hasil){
                            $('#kabupaten').html(hasil);
                            $('#modalMod [name="kabupaten"]').val(tr.data('kabupaten'));
                        } 
                    });
                }

                if(tr.data('kabupaten'))
                {
                    var idkb = tr.data('kabupaten');
                    $.ajax({
                        type : 'post',
                        data : 'id_kab='+idkb,
                        url : link+'/getkec',
                        success : function(hasil)
                        {
                            $('#kecamatan').html(hasil);
                            $('#modalMod [name="kecamatan"]').val(tr.data('kecamatan'));
                        }
                    });
                }

                if(tr.data('kecamatan'))
                {
                    var idds = tr.data('kecamatan');
                    $.ajax({
                        type : 'post',
                        data : 'id_kec='+idds,
                        url : link+'/getdesa',
                        success : function(hasil)
                        {
                          $('#desa').html(hasil);
                          $('#modalMod [name="kelurahan"]').val(tr.data('kelurahan'));
                        }
                    });
                }

                $('.sm-war-kosong').show();
                $('#modalMod').modal();
            }
            else
            {
                history.go(-1);
            }
         })

        $('[name="id[]"]').click(function () {
            //alert('dfds');
            if ($('[name="id[]"]:checked').length > 0) {
                $('#btnDelete').removeAttr('disabled');
            } else {
                $('#btnDelete').attr('disabled', 'true');
            }
        });

        $('.nip').change(function(){
            var nip = $(this).val();
            //alert(id_pegawai);
            $.ajax({
                type : 'post',
                url : link+'/getnip',
                data : 'nip='+nip,
                success : function(r){
                    if(r)
                    {
                        $('.txt_nip').css('background','red');
                        $('.txt_nip').html(r);
                    }
                    else
                    {
                        $('.txt_nip').css('background','white');
                        $('.txt_nip').html('');
                    }
                }
            });
        });

        $('.email').change(function(){
            var email = $(this).val();
            //alert(id_pegawai);
            $.ajax({
                type : 'post',
                url : link+'/getemail',
                data : 'email='+email,
                success : function(r){
                    if(r)
                    {
                        $('.txt_email').css('background','red');
                        $('.txt_email').html(r);
                    }
                    else
                    {
                        $('.txt_email').css('background','white');
                        $('.txt_email').html('');
                    }
                }
            });
        });
	});

	function formAdd() {
        if(checkPrivilege('<?php echo hak_akses('pegawai_add','add');?>'))
        {
            $('#modalMod :input').val('');
            $(document).on('change','select[name="provinsi"]',function(){
                var idpv = jQuery(this).val();
                $.ajax({
                    type  : 'post',
                    data  : 'id_prov='+idpv,
                    url   : link+'/getkab',
                    success : function(hasil){
                        $('#kabupaten').html(hasil);
                    } 
                });
            });

            $(document).on('change','select[name="kabupaten"]', function(){
                var idkb = $(this).val();

                $.ajax({
                    type : 'post',
                    data : 'id_kab='+idkb,
                    url : link+'/getkec',
                    success : function(hasil)
                    {
                        $('#kecamatan').html(hasil);
                    }
                });
            });

            $(document).on('change','select[name="kecamatan"]', function(){
                var idds = $(this).val();

                $.ajax({
                    type : 'post',
                    data : 'id_kec='+idds,
                    url : link+'/getdesa',
                    success : function(hasil)
                    {
                      $('#desa').html(hasil);
                    }
                });
            });

            $('.sm-war-kosong').hide();
            $('#modalMod').modal();
        }else{
            history.go(-1);
        }
    }

    function formDelete() {
        if(checkPrivilege('<?php echo hak_akses('pegawai_delete','delete');?>'))
        {
            if (confirm('Are you sure want to delete this data?')) {
                var pkC = $('[name="id[]"]:checked'), idC = [];

                for (var ip = 0; ip < pkC.length; ip++) {
                    idC.push(pkC.eq(ip).val());
                }

                window.location = '<?php echo base_url($this->uri->segment(1).'/delete/')?>' + (idC.join('k'));
            }
        }else{
            history.go(-1);
        }
    }
</script>
