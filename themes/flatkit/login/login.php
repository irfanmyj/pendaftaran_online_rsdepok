<?php  
  get_template('inc/header.html');
?>
<!-- ############ LAYOUT START-->
  <div class="center-block w-xxl w-auto-xs p-y-md">
    <div class="navbar">
      <div class="pull-center">
        <div ui-include="'../views/blocks/navbar.brand.html'"></div>
      </div>
    </div>
    <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
      <div class="m-b text-sm">
        Sign in with your Flatkit Account
      </div>
      <form name="form" method="post" action="<?php echo base_url('welc/c');?>">
        <div class="md-form-group float-label">
          <input type="text" name="username" class="md-input" required>
          <label>Username</label>
        </div>
        <div class="md-form-group float-label">
          <input type="password" name="password" class="md-input" required>
          <label>Password</label>
        </div>      
        <div class="m-b-md">        
          <label class="md-check">
            <input type="checkbox"><i class="primary"></i> Keep me signed in
          </label>
        </div>
        <button type="submit" class="btn primary btn-block p-x-md">Sign in</button>
      </form>
    </div>
  </div>
  <br/>
  <br/>
  <br/>
  <br/>
<!-- ############ LAYOUT END-->
<?php
  get_template('inc/js.html');
  get_template('inc/footer.html');
?>