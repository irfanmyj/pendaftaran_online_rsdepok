        <div class="p-y-md clearfix nav-active-white">
          <div class="nav-active-success">
            <ul class="nav nav-pills nav-sm" ui-nav>
              <li class="nav-item <?php echo (a('uri')=='home_view') ? ' active ' : ''; ?>"><a class="nav-link" href="<?php echo base_url('home/view'); ?>">Aktivitas</a></li>
              <li class="nav-item <?php echo (a('uri')=='pendidikan_personal') ? ' active ' : ''; ?>"><a class="nav-link"  href="<?php echo base_url('pendidikan/personal'); ?>">Pendidikan</a></li>
              <li class="nav-item"><a href class="nav-link">Diklat</a></li>
              <li class="nav-item"><a class="nav-link" href>Cuti</a></li>
              <li class="nav-item"><a class="nav-link" href>Absensi</a></li>
              <li class="nav-item"><a class="nav-link" href>Jadwal Kerja</a></li>
              <li class="nav-item"><a class="nav-link" href>Buka Tiket</a></li>
            </ul>
          </div>
        </div>