    <div class="item-bg">
      <img src="<?php echo @base_url('uploads/picture/'.getData('pegawai','foto',array('nip'=>$this->session->userdata('nip')))[0]->foto);?>" class="blur opacity-3">
    </div>
    <div class="p-a-md">
      <div class="row m-t">
        <div class="col-sm-7">
          <a href class="pull-left m-r-md">
            <span class="avatar w-96">
              <img src="<?php echo @base_url('uploads/picture/'.getData('pegawai','foto',array('nip'=>$this->session->userdata('nip')))[0]->foto);?>">
              <i class="on b-white"></i>
            </span>
          </a>
          <div class="clear m-b">
            <h3 class="m-0 m-b-xs"><?php echo $this->session->userdata('nm_lengkap'); ?></h3>
            <p class="text-muted"><span class="m-r"><?php echo $this->session->userdata('nip'); ?></span> <small><i class="fa fa-map-marker m-r-xs"></i><?php echo $this->session->userdata('tmpt_lahir').', '.tanggal_indo($this->session->userdata('tgl_lahir'),true); ?></small></p>
            <p class="text-muted">
              <span class="m-r">
                <?php echo getData('departement','nm_departement',array('id_departement'=>$this->session->userdata('id_departement')))[0]->nm_departement;?>
              </span>
            </p>
            <div class="block clearfix m-b">
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-facebook"></i>
                <i class="fa fa-facebook indigo"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-twitter"></i>
                <i class="fa fa-twitter light-blue"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-google-plus"></i>
                <i class="fa fa-google-plus red"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-linkedin"></i>
                <i class="fa fa-linkedin cyan-600"></i>
              </a>
            </div>
            <a href class="btn btn-sm warn btn-rounded m-b">Follow</a>
          </div>
        </div>
        <div class="col-sm-5">
          <p class="text-md profile-status">I am feeling good!</p>
          <button class="btn btn-sm white" data-toggle="collapse" data-target="#editor">Edit</button>
          <div class="collapse box m-t-sm" id="editor">
            <textarea class="form-control no-border" rows="2" placeholder="Type something..."></textarea>
          </div>
        </div>
      </div>
    </div>