<div class="modal fade animate" id="modalMod" style="display: none">
    <div class="modal-dialog modal-lg fade-left">
        <div class="modal-content">
            <div class="modal-header blue">
                <h4 class="modal-title">Manajemen Kepegawaian</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url($this->uri->segment(1).'/save') ?>" method="post" id="modForm" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" name="id">
                    <input type="hidden" name="old_image">

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Nip</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="nip" placeholder="Nomor Induk Pegawai" required="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Nama Lengkap</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="nm_lengkap" placeholder="Nama Lengkap" required="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Tanggal Lahir</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control input-sm" name="tgl_lahir">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Tempat Lahir</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="tmpt_lahir" placeholder="Tempat Lahir" required="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Telp</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control input-sm" name="telp" placeholder="Telp">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control input-sm" name="email" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Jenis Kelamin</label>
                        <div class="col-sm-12">
                        <?php
                        echo htmlSelectFromArray($jk, 'name="jk" id="jk" style="width:100%;" class="form-control select2"', true);
                        ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Golongan Darah</label>
                        <div class="col-sm-12">
                        <?php
                        echo htmlSelectFromArray($goldarah, 'name="gol_darah" id="gol_darah" style="width:100%;" class="form-control select2"', true);
                        ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Provinsi</label>
                        <div class="col-sm-12">
                        <?php
                        echo htmlSelectFromArray($provinsi, 'name="provinsi" id="provinsi" style="width:100%;" class="form-control select2"', true);
                        ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Kabupaten</label>
                        <div class="col-sm-12" id="kabupaten"></div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Kecamatan</label>
                        <div class="col-sm-12" id="kecamatan"></div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-3 control-label">Desa</label>
                        <div class="col-sm-12" id="desa"></div>
                    </div>

                    <div class="form-group">
                        <label for="tun" class="col-sm-12 control-label">Foto</label>
                        <div class="col-sm-12">
                        <input type="file" name="image" class="form-control">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" form="modForm" class="btn btn-sm white"><i class="fa fa-arrow-circle-right"></i> Save</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>