<?php defined('BASEPATH') OR exit('No direct script access allowed');

	if(!function_exists('get_template'))
	{
		function get_template($view){
			$_this =& get_instance();
			return $_this->site->view($view);
		}
	}