<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Infopublik extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function jadwal_dokter()
	{
		global $Cf;
		$hari = hari_indo(date('Y-m-d'));
		$res = $this->Get_model->getJadwalDokter($hari);
		$datas = array();
		foreach ($res as $k => $v) {
			$datas[$v->kd_dokter] = [
				'nm_dokter' => $v->nm_dokter, 
				'nm_poli' => $v->nm_poli, 
				'jam_mulai' => $v->jam_mulai, 
				'jam_selesai' => $v->jam_selesai, 
				'tgl_awal' => $v->tgl_awal, 
				'tgl_akhir' => $v->tgl_akhir, 
				'status' => $v->status
			];
		}
		
		$data = array(
			'title' => 'Info Jadwal Dokter',
			'r'	=> array_values($datas),
			'inc' => 'jadwal_dokter'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman jadwal dokter',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function jadwaldokter()
	{
		global $Cf;
		$hari = hari_indo(date('Y-m-d'));
		$res = $this->Get_model->getJadwalDokter($hari);
		$datas = array();
		foreach ($res as $k => $v) {
			$datas[$v->kd_dokter] = [
				'nm_dokter' => $v->nm_dokter, 
				'nm_poli' => $v->nm_poli, 
				'jam_mulai' => $v->jam_mulai, 
				'jam_selesai' => $v->jam_selesai, 
				'tgl_awal' => $v->tgl_awal, 
				'tgl_akhir' => $v->tgl_akhir, 
				'status' => $v->status
			];
		}
		
		$data = array(
			'title' => 'Info Jadwal Dokter',
			'r'	=> array_values($datas),
			'inc' => 'jadwal_dokter1'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman jadwal dokter ',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function cuti_dokter()
	{
		global $Cf;
		$dNow = date('Y-m') . (date('d')-6);
		$tgl_kedepan 	= AddTglNext($dNow,($Cf->_jadwal_cuti+20),'days');
		$where = array(
			'a.status'=> 'Y',
			'a.tgl_awal >='=> $dNow,
			'a.tgl_akhir <='=> $tgl_kedepan,
		);

		$res = $this->Get_model->getCutiDokter($where);

		$data = array(
			'title' => 'Info Jadwal Cuti Dokter',
			'r'	=> $res,
			'inc' => 'cuti_dokter'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman cuti dokter',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function cutidokter()
	{
		global $Cf;
		$dNow = date('Y-m-d');
		$tgl_kedepan 	= AddTglNext($dNow,$Cf->_hari_daftar,'days');

		$where = array(
			'a.status'=> 'Y',
			'a.tgl_awal >='=> $dNow,
			'a.tgl_akhir <='=> $tgl_kedepan,
		);

		$res = $this->Get_model->getCutiDokter($where);

		$data = array(
			'title' => 'Info Jadwal Cuti Dokter',
			'r'	=> $res,
			'inc' => 'cuti_dokter1'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman cuti dokter',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function kamar()
	{
		global $Cf;
		$res = $this->Get_model->getRecordList('aplicare_ketersediaan_kamar','SUM(tersedia) as total, kode_kelas_aplicare as nama,kd_bangsal','','kode_kelas_aplicare');

		$data = array(
			'title' => 'KETERSEDIAAN TEMPAT TIDUR RSUD KOTA DEPOK',
			'r' => $res,
			'kamar' => $Cf->_kd_bangsal,
			'inc' => 'kamar'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi kamar',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function infokamar()
	{
		global $Cf;
		$res = $this->Get_model->getRecordList('aplicare_ketersediaan_kamar','SUM(tersedia) as total, kode_kelas_aplicare as nama,kd_bangsal','','kode_kelas_aplicare');

		$data = array(
			'title' => 'KETERSEDIAAN TEMPAT TIDUR RSUD KOTA DEPOK',
			'r' => $res,
			'kamar' => $Cf->_kd_bangsal,
			'inc' => 'kamar1'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi kamar',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function tanya_jawab()
	{
		global $Cf;
		$data = array(
			'title' => 'Informasi Seputar Pertanyaan Umum',
			'judul'	=> $Cf->_judul_tanya_jawab,
			'isi'	=> $Cf->_jawaban,
			'inc' 	=> 'tanya_jawab'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi seputar pertanyaan umum',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function tanyajawab()
	{
		global $Cf;
		$data = array(
			'title' => 'Informasi Seputar Pertanyaan Umum',
			'judul'	=> $Cf->_judul_tanya_jawab,
			'isi'	=> $Cf->_jawaban,
			'inc' 	=> 'tanya_jawab1'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi seputar pertanyaan umum',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function info_faskes()
	{
		$data = array(
			'title' => 'Pencarian Informasi Faskes',
			'inc' 	=> 'infofaskes'
		);

		/*print_r($data);
		exit();*/
		$this->site->view('inc',$data);
	}

	public function infofaskes()
	{
		$data = array(
			'title' => 'Pencarian Informasi Faskes',
			'inc' 	=> 'infofaskes1'
		);

		/*print_r($data);
		exit();*/
		$this->site->view('inc',$data);
	}


	public function getfaskes()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Referensi($Cf->_confbpjs_tes);
			$p = $this->input->post();

			$res = $api->faskes(htmlentities($p['nm_daerah'],ENT_QUOTES),htmlentities($p['nm_faskes'],ENT_QUOTES));

			if($res['response']['faskes'])
			{
				echo '<div class="wrap-content b-shadow">';
					echo '<table class="table table-striped">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>No</th>';
								echo '<th>Kode Faskes</th>';
								echo '<th>Nama Faskes</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							$no = 0;
							foreach ($res['response']['faskes'] as $k => $v) {
								$no++;
								echo '<tr>';
									echo '<td>'.$no.'</td>';
									echo '<td>'.$v['kode'].'</td>';
									echo '<td><a href="https://www.google.com/search?q='.str_replace(" ", "+", $v['nama']).'" target="_blank">'.$v['nama'].'</a></td>';
								echo '</tr>';
							}
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
			}else
			{
				echo '<label class="blue">
					  <h3>Data tidak ditemukan pada server BPJS.</h3>
					</label>';
			}
		}
		else
		{
			$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
			);
			$this->site->view_error('error_404',$data);
		}
	}
}
