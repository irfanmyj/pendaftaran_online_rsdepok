<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		global $Cf;
		if(hak_akses(a('uri'),a())==FALSE)
		{
			$_SESSION['error'] = 'info';
			$_SESSION['msg']   = 'Mohon maaf anda tidak mempunyai akses pada module ini.';
			redirect(base_url('welc/auth'));
		}
		
		$f 		= ($this->uri->segment(3))	? $this->uri->segment(3) : 'f';
		$l 		= ($this->uri->segment(4))	? $this->uri->segment(4) : 10;
		$p 		= ($this->uri->segment(5))	? $this->uri->segment(5) - 1 : 0;

		$pq 	= $p * $l;
		$no 	= ($p * $l) + 1;

		$where 	= '';
		if($f!='f')
		{
			$where = 'bahasa like \'%'.$f.'%\'';
		}

		$jum 	= $this->Get_model->countCustom('bahasa',$where);
		$r 		= $this->Get_model->getRecordList('bahasa','*',$where,'','id_bahasa ASC',$l,$pq);

		$rp		= $this->Get_model->getRecordList('pegawai','id_pegawai,nm_lengkap');
		$lsrp	= ConDataToArray($rp,'id_pegawai','nm_lengkap');

		$data = array(
			'title'		=> 'Manajemen Bahasa',
			'f' 		=> $f,
			'l' 		=> $l,
			'p' 		=> $p,
			'pq' 		=> $pq,
			'no' 		=> $no,
			'jum' 		=> $jum,
			'r'			=> $r,
			'limit'		=> $Cf->_limit,
			'jenis_bahasa'=> $Cf->_jenis_bahasa,
			'st_bahasa'	=> $Cf->_st_bahasa,
			'pegawai'	=> $lsrp,
			'inc'		=> 'bahasa'
		);

		$this->site->view('inc',$data);
	}

	public function save()
	{
		$p = $this->input->post();

		$dt['id_pegawai']		= isset($p['id_pegawai']) ? $p['id_pegawai'] : '';
		$dt['id_jenis_bahasa']	= isset($p['id_jenis_bahasa']) ? $p['id_jenis_bahasa'] : '';
		$dt['bahasa']			= isset($p['bahasa']) ? $p['bahasa'] : '';
		$dt['status']			= isset($p['status']) ? $p['status'] : '';
		$dt['masuk_oleh']		= $this->session->userdata('id_user');
		$dt['masuk_tgl']		= date('Y-m-d H:i:s');

		$rules 					= $this->Get_model->bahasa;
		$this->form_validation->set_rules($rules);
		
		if($p['id'])
		{
			if($this->form_validation->run() == FALSE)
			{
				$_SESSION['error'] = '1';
				$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil diperbarui, silakan coba lagi nanti.';
				$this->Get_model->createHistory('Gagal update nama bahasa dengan nama : '.$p['nama'].'',$this->session->userdata('id_user'));
				generateReturn(base_url('bahasa/view'));
			}
			else
			{
				$_SESSION['error'] = '';
				$_SESSION['msg']   = 'Data telah berhasil diperbarui.';
				$this->Get_model->createHistory('Update nama bahasa dengan nama : '.$p['bahasa'].'',$this->session->userdata('id_user'));
				$this->Get_model->update('bahasa',$dt,array('id_bahasa'=>$p['id']));
				generateReturn(base_url('bahasa/view'));
			}
		}
		else
		{
			if($this->form_validation->run() == FALSE){
				$_SESSION['error'] = '1';
				$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil disimpan, silakan coba lagi nanti.';
				$this->Get_model->createHistory('Gagal tambah nama bahasa dengan nama : '.$p['bahasa'].'',$this->session->userdata('id_user'));
				$this->Get_model->createHistory('Gagal tambah nama bahasa dengan nama : '.$p['bahasa'].'',$this->session->userdata('id_user'));
				generateReturn(base_url('bahasa/view'));
			}
			else
			{
				$_SESSION['error'] = '';
				$_SESSION['msg']   = 'Data telah berhasil simpan.';
				
				$this->Get_model->createHistory('Tambah nama bahasa dengan nama : '.$p['bahasa'].'',$this->session->userdata('id_user'));
				$this->Get_model->insert('bahasa',$dt,FALSE);
				generateReturn(base_url('bahasa/view'));
			}
		}
	}

	public function delete()
	{
		$uri = $this->uri->segment(3);
		$idl = explode('k', $uri);

		foreach ($idl as $idv)
		{
			$det = $this->Get_model->getRecordList('bahasa','bahasa','id_bahasa=\''.$idv.'\'');
			if($this->Get_model->delete('bahasa',array('id_bahasa'=>$idv)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
			}
			else
			{
				$_SESSION['error'] = '';
        		$_SESSION['msg']   = 'Data telah berhasil dihapus';
				$this->Get_model->createHistory('Delete bahasa dengan nama : '.$det[0]->bahasa.'',$this->session->userdata('ID'));
			}
			//echo $det[0]->nm_level;
		}

		generateReturn(base_url('bahasa/view'));
	}
}
