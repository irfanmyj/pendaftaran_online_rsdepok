<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function list()
	{
		global $Cf;
		$post 	= $this->input->post();
		$search	= isset($post['search']) ? $post['search'] : '';
		$limit 	= isset($post['limit']) ? $post['limit'] : $Cf->default_limit;
		$offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		
		$where = '';
		if($search)
		{
			$where = 'judul like \'%'.$search.'%\'';
		} 

		$r3 		= $this->Get_model->getArtikel('tbl_artikel','*',$where,'','dilihat DESC',3);
		$r 		= $this->Get_model->getArtikel('tbl_artikel','*',$where,'','id_artikel DESC',$limit,$offset);
		$count = $this->Get_model->countArtikel('tbl_artikel',$where);
		pagination(site_url($this->uri->segment(1).'/list'),$count,$limit);

		$data = array(
			'title'	=> 'Artikel',
			'r' => $r,
			'rr' => $r3,
			'inc'=> 'blog'
		);

		$this->site->view('inc',$data);
	}

	public function detail()
	{
		/*print_r($this->session->userdata);
		exit();*/
		$id = $this->uri->segment(3);
		$res = $this->Get_model->getArtikel('tbl_artikel','*',array('id_artikel' => $id));
		$res1 = $this->Get_model->getArtikel('tbl_artikel','*','','','id_artikel DESC',2);

		$datas['dilihat'] = $res[0]->dilihat + 1;
		$where = array(
			'id_artikel' => $id
		);
		$this->Get_model->updateArtikel('tbl_artikel',$datas,$where);
		
		$data = array(
			'title'	=> 'Detail Artikel',
			'r' => $res,
			'rr' => $res1,
			'inc'=> 'detailblog'
		);

		$this->site->view('inc',$data);
	}
}
