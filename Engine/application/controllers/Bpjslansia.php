<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bpjslansia extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		unset($_SESSION['tgl_registrasi']);
		unset($_SESSION['id_poli']);
		unset($_SESSION['id_poli1']);
		unset($_SESSION['id_poli2']);
		unset($_SESSION['no_rujukan']);
		unset($_SESSION['no_skdp']);

		$data = array(
			'title'		=> 'Booking Menggunakan BPJS',
			'inc'		=> 'lansiabookingbpjs'
		);

		$this->site->view('inc',$data);
	}

	public function booking()
	{
		global $Cf;
		unset($_SESSION['tgl_registrasi']);
		unset($_SESSION['id_poli']);
		unset($_SESSION['id_poli1']);
		unset($_SESSION['id_poli2']);
		unset($_SESSION['no_rujukan']);

		$data = array(
			'title'		=> 'Booking Dengan No Rujukan',
			'inc'		=> 'lansiabpjs'
		);
		$this->site->view('inc',$data);
	}

	public function bpjs2()
	{
		global $Cf;
		$tgl_registrasi = isset($_SESSION['tgl_registrasi']) ? $_SESSION['tgl_registrasi'] : '';
		$id_poli1 		= isset($_SESSION['id_poli']) ? $_SESSION['id_poli'] : '';
		$id_poli2 		= isset($_SESSION['id_poli']) ? $_SESSION['id_poli'].'S' : '';
		$no_rujukan 	= isset($_SESSION['no_rujukan']) ? $_SESSION['no_rujukan'] : '';
		$hari 			= hari_indo($tgl_registrasi);

		if($tgl_registrasi =='' && $id_poli1 =='' && $id_poli2 =='')
		{
			generateReturn(base_url('bpjslansia/booking'));
		}else
		{
			$where = 'a.hari_kerja=\''.$hari.'\'';

			$poli = $this->Get_model->getKunjunganPoli($where);
			if($poli)
			{
				$conp = ConDataToArray($poli,'kd_poli','nm_poli');
			}
			else
			{
				$polis = $this->Get_model->getKunjunganPoli();
				$conp = ConDataToArray($polis,'kd_poli','nm_poli');
			}

			if($this->session->userdata('usia') >= 17)
			{
				unset($conp['ANA']);
			}

			$dokter = array();

			$data = array(
				'title'		=> 'Booking Bpjs Form ke 2',
				'poli'		=> $conp,
				'hari'		=> $hari,
				'dokter'	=> $dokter,
				'inc'		=> 'lansiabpjs2'
			);

			$this->site->view('inc',$data);
		}
		
	}

	public function batal()
	{
		global $Cf;
		$dNow = date('Y-m-d');
		$tgl = AddTglNext($dNow,$Cf->_hari_daftar,'days');

		$where = array(
				'a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),
				'a.tanggal_periksa >='=>$dNow,
				'a.tanggal_periksa <='=>$tgl,
				'a.status'=>'Belum',
				'a.kd_pj' => 'BPJ'
				);
		$res = $this->Get_model->getRiwayatBooking($where);
		
		$data = array(
			'title'		=> 'Pembatalan Booking BPJS',
			'r'			=> $res,
			'inc'		=> 'lansiabatalbpjs'
		);
		$this->site->view('inc',$data);
	}

	public function rawatinap()
	{
		global $Cf;
		unset($_SESSION['tgl_registrasi']);
		unset($_SESSION['id_poli']);
		unset($_SESSION['id_poli1']);
		unset($_SESSION['id_poli2']);
		//unset($_SESSION['no_rujukan']);

		$data = array(
			'title'		=> 'Booking Dengan No Rujukan',
			'inc'		=> 'lansiabpjsri'
		);
		$this->site->view('inc',$data);
	}

	public function bpjsri2()
	{
		global $Cf;
		$tgl_registrasi = isset($_SESSION['tgl_registrasi']) ? $_SESSION['tgl_registrasi'] : '';
		$id_poli1 		= isset($_SESSION['id_poli']) ? $_SESSION['id_poli'] : '';
		$id_poli2 		= isset($_SESSION['id_poli']) ? $_SESSION['id_poli'].'S' : '';
		//$no_rujukan 	= isset($_SESSION['no_rujukan']) ? $_SESSION['no_rujukan'] : '';
		$no_sep 		= isset($_SESSION['no_sep']) ? $_SESSION['no_sep'] : '';
		$hari 			= hari_indo($tgl_registrasi);

		if($tgl_registrasi =='' && $id_poli1 =='' && $id_poli2 =='')
		{
			generateReturn(base_url('bpjs/booking'));
		}
		else
		{
			if($id_poli1=='' && $id_poli2=='')
			{
				$where = 'a.hari_kerja=\''.$hari.'\' and a.kd_poli=\''.$id_poli1.'\' or a.kd_poli=\''.$id_poli2.'\'';
			}
			else
			{
				$where = 'a.hari_kerja=\''.$hari.'\'';
			}

			$poli = $this->Get_model->getKunjunganPoli($where);
			$conp = ConDataToArray($poli,'kd_poli','nm_poli');
			$dokter = array();

			$data = array(
				'title'		=> 'Booking Bpjs Form ke 2',
				'poli'		=> $conp,
				'dokter'	=> $dokter,
				'hari'		=> $hari,
				'inc'		=> 'lansiabpjsri2'
			);

			$this->site->view('inc',$data);
		}

	}
}