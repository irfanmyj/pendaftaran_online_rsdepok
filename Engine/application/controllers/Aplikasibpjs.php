<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Aplikasibpjs extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		$data = array(
			'title'		=> 'Informasi Seputar Bpjs',
			'inc'		=> 'aplikasibpjs'
		);

		$this->site->view('inc',$data);
	}

	public function tes()
	{
		global $Cf;
		$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
		$res = $api->cariByNoRujukan('','013800010119P000001');
		echo '<pre>';
		print_r($res);
	}

}
