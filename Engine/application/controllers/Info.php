<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function jadwal_dokter()
	{
		global $Cf;
		$hari = hari_indo(date('Y-m-d'));
		$res = $this->Get_model->getJadwalDokter($hari);
		$datas = array();
		foreach ($res as $k => $v) {
			$datas[$v->kd_dokter] = [
				'nm_dokter' => $v->nm_dokter, 
				'nm_poli' => $v->nm_poli, 
				'jam_mulai' => $v->jam_mulai, 
				'jam_selesai' => $v->jam_selesai, 
				'tgl_awal' => $v->tgl_awal, 
				'tgl_akhir' => $v->tgl_akhir, 
				'status' => $v->status
			];
		}
		$data = array(
			'title' => 'Info Jadwal Dokter',
			'r'	=> array_values($datas),
			'inc' => 'jadwal_dokter'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman jadwal dokter',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function cuti_dokter()
	{
		global $Cf;
		$dNow = date('Y-m-d', strtotime("-6 day", strtotime(date("Y-m-d"))));
		$tgl_kedepan 	= AddTglNext($dNow,($Cf->_jadwal_cuti+20),'days');
		
		$where = array(
			'a.status'=> 'Y',
			'a.tgl_awal >='=> $dNow,
			'a.tgl_akhir <='=> $tgl_kedepan,
		);

		$res = $this->Get_model->getCutiDokter($where);

		$data = array(
			'title' => 'Info Jadwal Cuti Dokter',
			'r'	=> $res,
			'inc' => 'cuti_dokter'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman cuti dokter',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function kamar()
	{
		global $Cf;
		$res = $this->Get_model->getRecordList('aplicare_ketersediaan_kamar','SUM(tersedia) as total, kode_kelas_aplicare as nama,kd_bangsal','','kode_kelas_aplicare');

		$data = array(
			'title' => 'KETERSEDIAAN TEMPAT TIDUR RSUD KOTA DEPOK',
			'r' => $res,
			'kamar' => $Cf->_kd_bangsal,
			'inc' => 'kamar'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi kamar',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function maps()
	{
		$data = array(
			'title' => 'Info Maps',
			'maps' => '<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.925009840376!2d106.75610734958593!3d-6.4036628953438886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e8e7cb10676b%3A0x50367589ec61dde7!2sRSUD+Depok!5e0!3m2!1sid!2sid!4v1548124408366"></iframe>',
			'inc' => 'maps'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman maps',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function riwayat_booking()
	{
		$res1 = $this->Get_model->getRiwayatBooking();
		$res2 = $this->Get_model->getRegPeriksa();

		$data = array(
			'title' => 'Riwayat Booking',
			'r1'	=> $res1,
			'r2'	=> $res2,
			'inc' => 'riwayat_booking'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman riwayat booking',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function tanya_jawab()
	{
		global $Cf;
		$data = array(
			'title' => 'Informasi Seputar Pertanyaan Umum',
			'judul'	=> $Cf->_judul_tanya_jawab,
			'isi'	=> $Cf->_jawaban,
			'inc' 	=> 'tanya_jawab'
		);

		// Update Aktivitas
		$this->Get_model->createHistory('Anda '.$this->session->userdata('nm_lengkap').' telah membuka halaman informasi seputar pertanyaan umum',$this->session->userdata('no_rkm_medis'));

		$this->site->view('inc',$data);
	}

	public function profile()
	{
		//global $Cf;
		$res = $this->Get_model->getAktivitas(array('DATE(activity_date)'=>date('Y-m-d'),'id_user'=>$this->session->userdata('no_rkm_medis')));
		$jml_kpoli = $this->Get_model->countCustom('reg_periksa',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'status_lanjut'=>'Ralan'));
		$jml_kranap = $this->Get_model->countCustom('reg_periksa',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'status_lanjut'=>'Ranap'));
		$jml_login = $this->Get_model->countCustom('user_account_login_log',array('id_user'=>$this->session->userdata('no_rkm_medis')));

		$jml = 0;
		$jumlah=array();
		$poli=array();
		$getSts = $this->Get_model->getSts(date('Y-m-d'));

		foreach ($getSts as $k => $v) {
			$jml += $v['jumlah'];
			$jumlah[] = intval($v['jumlah']);
    		$poli[] = $v['nm_poli'];
		}
	
		$data = array(
			'title' => 'Informasi Data Pribadi',
			'r'		=> $res,
			'jkp'	=> $jml_kpoli,
			'jkr'	=> $jml_kranap,
			'jkl'	=> $jml_login,
			'jmlt'	=> $jml,
			'jmlk'	=> $jumlah,
			'jmlp'	=> $poli,
			'inc' 	=> 'profile'
		);

		$this->site->view('inc',$data);
	}

	public function qr_code()
	{
		$data = array(
			'title' => 'Informasi Data Pribadi',
			'inc' 	=> 'qrcode'
		);

		$this->site->view('inc',$data);
	}

	public function notifikasi()
	{
		$where = array(
			'd.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),
			'a.status'=>'Y',
			'd.status'=>'Dokter Berhalangan'
		);

		$res = $this->Get_model->GetNotifikasi($where);
		$data = array(
			'title' => 'Informasi Brodcast',
			'r' => $res,
			'inc' 	=> 'info'
		);

		$this->site->view('inc',$data);
	}

	public function viewprofile($no_rm='')
	{
		$get = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis' => $no_rm),'','',1,'',2);

		$data = array(
			'title' => 'Data Profile',
			'row' => $get,
			'inc' => 'dataprofile'
		);

		$this->site->view('inc',$data);
	}

	public function getConten()
	{
		$r = file_get_contents('http://rsud.depok.go.id/?cat=13');
		//echo "<div>";
		preg_match_all('#<divclass="content-area(.+?)>(.+?)</div>#siu', $r, $result);
		//print_r($result);
		for ($x = 0, $jumlah = count($result[0]); $x < $jumlah; $x++) {
		echo $result[2][$x] . "n";
		}
		echo "</div>";
		/*preg_match_all('<header class="entry-header">', $r, $result);
		for ($x = 0, $jumlah = count($result[0]); $x < $jumlah; $x++) {
		echo $result[2][$x] . "n";
		}
		echo "</header></div>";*/
	}

	public function kuota()
	{
		$dNow = date('Y-m-d');
		$cdNow = hari_indo($dNow);
		$res = $this->Get_model->getKunjunganPoli(array('hari_kerja'=>$cdNow));
		$poli = ConDataToArray($res,'kd_poli','nm_poli');
		$data = array(
			'title' => 'Informasi Kuota Pendaftaran Online',
			'poli' => $poli,
			'inc' => 'info_kuota'
		);

		$this->site->view('inc',$data);
	}

	public function get_info_kuota()
	{
		$dNow = date('Y-m-d');
		$cdNow = hari_indo($dNow);
		$res1 = $this->Get_model->getInfoKuota1($cdNow);
		$res2 = $this->Get_model->getInfoKuota2($dNow);
		$res3 = $this->Get_model->getInfoKuota3($cdNow);
		$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$dNow),'','',1);
		$jum = count($res1);
		
		foreach ($res1 as $key => $value) {
			$d1[$value['kd_poli']][$value['kd_dokter']] = $value;
		}

		foreach ($res2 as $key => $value) {
			$d2[$value['kd_poli']][$value['kd_dokter']]['kuota_terpakai'] = $value['limit_reg'];
			$d2[$value['kd_poli']][$value['kd_dokter']]['tanggal_periksa'] = $value['tanggal_periksa'];
		}

		foreach ($res3 as $key => $value) {
			$d4[$value['kd_poli']][$value['kd_dokter']]['tanggal_libur'] = ($value['tanggal']) ? $value['tanggal'] : '';
		}

		$d3 = array_merge_recursive($d1,$d2,$d4);

		if(@$getLiburNasional[0]->tanggal=='')
		{
			$no =1;
	        foreach ($d3 as $k1 => $v1) {
	            foreach ($v1 as $k2 => $v2) {
	                $kuota_terpakai = isset($v2['kuota_terpakai']) ? $v2['kuota_terpakai'] : 0;
	                $sisa = @$v2['limit_reg'] - $kuota_terpakai;
	                if($sisa>0 && @$v2['tanggal_libur']=="")
	                {
	                    echo '<tr>';
	                        echo '<td>'.($no++).'</td>';
	                        echo '<td>'.@$dNow.'</td>';
	                        echo '<td>'.$v2['hari_kerja'].'</td>';
	                        echo '<td>'.$v2['nm_poli'].'</td>';
	                        echo '<td>'.$v2['nm_dokter'].'</td>';
	                        echo '<td style="text-align:center;">'.$v2['limit_reg'].'</td>';
	                        echo '<td style="text-align:center;">'.$sisa.'</td>';
	                        echo '<td style="text-align:center;">';
	                        if($sisa <= 3)
                            {
                                 echo '<button type="button" data-kd_poli="'.$v2['kd_poli'].'" data-kd_dokter="'.$v2['kd_dokter'].'" data-tanggal_periksa="'.$dNow.'" class="btn btn-warning btn-sm" id="daftar_langsung">Daftar Langsung</button>';
                            }else{
                                echo '<button type="button" data-kd_poli="'.$v2['kd_poli'].'" data-kd_dokter="'.$v2['kd_dokter'].'" data-tanggal_periksa="'.$dNow.'" class="btn btn-primary btn-sm" id="daftar_langsung">Daftar Langsung</button>';
                            }
	                        echo '</td>';
	                    echo '<tr>';
	                }
	            }
	        }
		}
		else
		{
			echo '<tr>';
				echo '<td colspan="8">'.$getLiburNasional[0]->keterangan.'</td>';
			echo '</tr>';
		}
	}

	public function search_info_kuota()
	{
		$post = $this->input->post();

		$dNow 	= date('Y-m-d',strtotime($post['tanggal_periksa']));
		$cdNow 	= hari_indo($dNow);
		$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$dNow),'','',1);
		$res1	= $this->Get_model->searchInfoKuota1($cdNow,$post['kd_poli']);
		$res2 	= $this->Get_model->searchInfoKuota2($post['tanggal_periksa'],$post['kd_poli']);
		$res3 	= $this->Get_model->searchInfoKuota3($cdNow,$post['tanggal_periksa']);

		foreach ($res1 as $key => $value) {
			$d1[$value['kd_poli']][$value['kd_dokter']] = $value;
		}

		foreach ($res2 as $key => $value) {
			$d2[$value['kd_poli']][$value['kd_dokter']]['kuota_terpakai'] = $value['limit_reg'];
			$d2[$value['kd_poli']][$value['kd_dokter']]['tanggal_periksa'] = $value['tanggal_periksa'];
		}

		foreach ($res3 as $key => $value) {
			$d4[$value['kd_poli']][$value['kd_dokter']]['tanggal_libur'] = ($value['tanggal']) ? $value['tanggal'] : '';
		}

		$d3 = [];
		if(isset($d2))
		{
			if(@$d4)
			{
				$d3 = array_merge_recursive($d1,$d2,$d4);
			}
			else
			{
				$d3 = array_merge_recursive($d1,$d2);
			}
			
		}

		$res = $this->Get_model->getKunjunganPoli(array('hari_kerja'=>$cdNow));
		$poli = ConDataToArray($res,'kd_poli','nm_poli');
		$data = array(
			'title' => 'Informasi Kuota Pendaftaran Online',
			'poli' => $poli,
			'kd_poli' => $post['kd_poli'],
			'tanggal_periksa' => $post['tanggal_periksa'],
			'getLiburNasional' => $getLiburNasional,
			'd3' => $d3,
			'inc' => 'search_info_kuota'
		);

		$this->site->view('inc',$data);
	}

	public function get_info_kuota2()
	{
		$post = $this->input->post();

		$dNow 	= date('Y-m-d',strtotime($post['tanggal_periksa']));
		$cdNow 	= hari_indo($dNow);
		$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$dNow),'','',1);
		$res1	= $this->Get_model->searchInfoKuota1($cdNow,$post['kd_poli']);
		$res2 	= $this->Get_model->searchInfoKuota2($post['tanggal_periksa'],$post['kd_poli']);
		$res3 = $this->Get_model->searchInfoKuota3($cdNow,$post['tanggal_periksa']);

		foreach ($res1 as $key => $value) {
			$d1[$value['kd_poli']][$value['kd_dokter']] = $value;
		}

		foreach ($res2 as $key => $value) {
			$d2[$value['kd_poli']][$value['kd_dokter']]['kuota_terpakai'] = $value['limit_reg'];
			$d2[$value['kd_poli']][$value['kd_dokter']]['tanggal_periksa'] = $value['tanggal_periksa'];
		}

		foreach ($res3 as $key => $value) {
			$d4[$value['kd_poli']][$value['kd_dokter']]['tanggal_libur'] = ($value['tanggal']) ? $value['tanggal'] : '';
		}

		$d3 = [];
		if(isset($d2))
		{
			if(@$d4)
			{
				$d3 = array_merge_recursive($d1,$d2,$d4);
			}
			else
			{
				$d3 = array_merge_recursive($d1,$d2);
			}
		}


		if(@$getLiburNasional[0]->tanggal=='')
        {
            $no =1;
            foreach ($d3 as $k1 => $v1) {
                foreach ($v1 as $k2 => $v2) {
                    $kuota_terpakai = isset($v2['kuota_terpakai']) ? $v2['kuota_terpakai'] : 0;
                    $sisa = @$v2['limit_reg'] - $kuota_terpakai;
                    if($sisa>0 && @$v2['tanggal_libur']=='')
                    {
                        echo '<tr>';
                            echo '<td>'.($no++).'</td>';
                            echo '<td>'.@$dNow.'</td>';
                            echo '<td>'.$v2['hari_kerja'].'</td>';
                            echo '<td>'.$v2['nm_poli'].'</td>';
                            echo '<td>'.$v2['nm_dokter'].'</td>';
                            echo '<td style="text-align:center;">'.$v2['limit_reg'].'</td>';
                            echo '<td style="text-align:center;">'.$sisa.'</td>';
                            echo '<td style="text-align:center;">';
                            if($sisa <= 3)
                            {
                                 echo '<button type="button" data-kd_poli="'.$v2['kd_poli'].'" data-kd_dokter="'.$v2['kd_dokter'].'" data-tanggal_periksa="'.$dNow.'" class="btn btn-warning btn-sm" id="daftar_langsung">Daftar Langsung</button>';
                            }else{
                                echo '<button type="button" data-kd_poli="'.$v2['kd_poli'].'" data-kd_dokter="'.$v2['kd_dokter'].'" data-tanggal_periksa="'.$dNow.'" class="btn btn-primary btn-sm" id="daftar_langsung">Daftar Langsung</button>';
                            }
                            echo '</td>';
                        echo '<tr>';
                    }
                }
            }
        }
        else
        {
            echo '<tr>';
                echo '<td colspan="8">'.$getLiburNasional[0]->keterangan.'</td>';
            echo '</tr>';
        }
	}

	public function checking_reg()
	{
		global $Cf;
		$post = $this->input->post();
		$tgl_registrasi	= date('Y-m-d',strtotime($post['tanggal_periksa']));
		$dNow 			= date('Y-m-d');
		$tNow 			= date('H:i:s');
		$tgl_kedepan 	= AddTglNext($dNow,$Cf->_hari_daftar,'days');

		$getBooking		= $this->Get_model->getInfoBooking(array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$tgl_registrasi,'a.limit_reg'=>1));

		$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$tgl_registrasi),'','',1);

		$getCekBk		= $this->Get_model->getInfoBooking(array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$post['tanggal_periksa'],'a.status'=>'Belum'));

		$sts = '';
		$msg = '';
		if($getCekBk[0]->no_reg)
		{
			$sts = 1;
			$msg = 'Mohon maaf anda sudah terdaftar pada poliklinik : '.@$getCekBk[0]->nm_poli.' pada Dokter : '. $getCekBk[0]->nm_dokter.' untuk tanggak kunjungan '. $getCekBk[0]->waktu_kunjungan;
		}
		else
		{
			if($tgl_registrasi == $dNow && $tNow > $Cf->_limit_jam)
			{
				$sts = 1;
				$msg = 'Mohon maaf pendaftaran sudah selesai';
			}
			else if($tgl_registrasi < $dNow)
			{
				$sts = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih kurang dari tanggal saat ini.';
			}
			else if($tgl_registrasi > $tgl_kedepan)
			{
				$sts = 1;
				$msg = 'Mohon maaf tanggal yang anda pilih melebihi batas limit pendaftaran, silahkan mundurkan tanggal kunjungan anda.';
			}
			else if($getBooking[0]->status == 'Belum')
			{
				$sts = 1;
				$msg = 'Anda sudah terdaftar '. tanggal_indo($getBooking[0]->tanggal_periksa,true) . ', pada Dokter '. $getBooking[0]->nm_dokter . ' ke '. $getBooking[0]->nm_poli . ' dengan waktu kunjungan '. tanggal_indo($getBooking[0]->waktu_kunjungan,true);
			}
			else if($getBooking[0]->status == 'Batal')
			{
				$sts = 1;
				$msg = 'Mohon maaf anda tidak bisa mendaftar pada tanggal yang sama, tercatat anda sudah mendaftar pada tanggal sekarang kemudian membatalkan.';
			}
			else if($getLiburNasional)
			{
				$sts = 1;
				$msg = 'Mohon maaf pada '. tanggal_indo($getLiburNasional[0]->tanggal) . ' '. $getLiburNasional[0]->keterangan;
			}			
		}

		echo json_encode(['sts'=>$sts,'msg'=>$msg]);
	}

	public function save_umum(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = $p['tanggal_periksa'];
			$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
			$kd_pj 			= $p['kd_pj'];
			$hari_kerja		= hari_indo($tgl_registrasi);
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'UMU','limit_reg'=>1,'status'=>'Belum'));
			
			$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
			$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

			// Get No Reg Terbesar Pada tabel booking_registrasi
			$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Get No Reg Terbesar Pada tabel reg_periksa
			$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
			$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
			$conNoReg 	= substr($NoReg, 0, 3);
			$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
			
			// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

           // get Jam Praktek
            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

            // Perkalian limit dan no registrasi
            $menitKedatangan = $limit_kedatangan * $NoRegNow;

            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
			$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

			// Pengurangan 30 menit sebelum praktek
			$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
			date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
			$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

			// Penambahan waktu/menit setiap pasien
			$TglPengurangan = date_create($TglKedatangan);
			date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
			$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

			$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
			$jam_datang = date('H:i:s',strtotime($waktu_datang));
			$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

			/*
			Info 	: Query get booking
			Manfaat : untuk cek apakah pasien sudah terdaftar pada tanggal yang dia pilih atau belum, karena dalam satu hari yang sama pasien tidak bisa booking ke dua tempat.
			*/
			$getCekBk		= $this->Get_model->getInfoBooking(array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$tgl_registrasi,'a.status'=>'Belum'));

			if($getCekBk[0]->no_reg)
			{
				$sts = 1;
				$msg = 'Mohon maaf anda sudah terdaftar pada poliklinik : '.@$getCekBk[0]->nm_poli.' pada Dokter : '. $getCekBk[0]->nm_dokter.' untuk tanggak kunjungan '. $getCekBk[0]->waktu_kunjungan;
			}else{
				if($getJumPendaftar[0]->total >= $limit)
				{
					$sts = 1;
					$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
				}
				else
				{
					$datas = array(
						'tanggal_booking' => $dNow,
						'jam_booking' => $tNow,
						'no_rkm_medis' => $no_rkm_medis,
						'tanggal_periksa' => $tgl_registrasi,
						'kd_dokter' => $kd_dokter,
						'kd_poli' => $kd_poli,
						'no_reg' => $NoRegNow,
						'kd_pj' => $kd_pj,
						'limit_reg' => 1,
						'waktu_kunjungan' => $waktu_datang,
						'status' => 'Belum'
					);

					if($this->Get_model->insert('booking_registrasi',$datas,FALSE))
					{
						$sts = 1;
						$msg = 'Mohon maaf data anda tidak bisa di simpan, telah terjadi kesalahan dalam system kami, silahkan ulangi lagi kemabli.';
					}
					else
					{
						$whRecordBooking 	= array(
							'tanggal' => $tgl_registrasi,
							'kd_dokter' => $kd_dokter,
							'kd_poli' => $kd_poli,
							'subkat' => 'Online',
							'kategori' => 'belum'
						);
						$getRecodBooking = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$whRecordBooking);

						$report_log = array();
						if($getRecodBooking[0]->total > 0)
						{
							$report_log['jumlah']	= $getRecodBooking[0]->total + 1;
							$this->Get_model->update('report_pendaftaran_online',$report_log,$whRecordBooking);
						}
						else
						{
							$report_log['tanggal']	= $tgl_registrasi;
							$report_log['kd_dokter']= $kd_dokter;
							$report_log['kd_poli']= $kd_poli;
							$report_log['kategori']	= "belum";
							$report_log['subkat']	= "Kuota";
							$report_log['jumlah']	= 1;
							$this->Get_model->insert('report_pendaftaran_online',$report_log);
						}

						$html = '<table class="table table-striped">';
							$html .= '<tr>';
								$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>No Uru Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$NoRegNow.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nomor Rekam Medik</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$no_rkm_medis.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nama Lengkap</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Periksa</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Waktu Datang</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Poliklinik Tujuan</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Dokter </td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Cara Bayar</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>Umum</td>';
							$html .= '</tr>';
						$html .= '</table">';

						$sts = 2;
						$msg = $html;
					}
				}
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function save_bpjs(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = ($p['tanggal_periksa']) ? $p['tanggal_periksa'] : '';
			$no_rujukan 	= ($p['no_rujukan']) ? $p['no_rujukan'] : '';
			$no_skdp 		= ($p['no_skdp']) ? $p['no_skdp'] : '000001';
			$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
			$kd_pj 			= 'BPJ';
			$hari_kerja		= hari_indo($tgl_registrasi);
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'BPJ','limit_reg'=>1,'status'=>'Belum'));
			
			$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
			$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

			// Get No Reg Terbesar Pada tabel booking_registrasi
			$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Get No Reg Terbesar Pada tabel reg_periksa
			$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
			$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
			$conNoReg 	= substr($NoReg, 0, 3);
			$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
			
			// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

           // get Jam Praktek
            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

            // Perkalian limit dan no registrasi
            $menitKedatangan = $limit_kedatangan * $NoRegNow;

            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
			$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

			// Pengurangan 30 menit sebelum praktek
			$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
			date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
			$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

			// Penambahan waktu/menit setiap pasien
			$TglPengurangan = date_create($TglKedatangan);
			date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
			$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

			$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
			$jam_datang = date('H:i:s',strtotime($waktu_datang));
			$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

			if($getJumPendaftar[0]->total >= $limit)
			{
				$sts = 1;
				$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
			}
			else
			{
				$datas = array(
					'tanggal_booking' => $dNow,
					'jam_booking' => $tNow,
					'no_rkm_medis' => $no_rkm_medis,
					'tanggal_periksa' => $tgl_registrasi,
					'kd_dokter' => $kd_dokter,
					'kd_poli' => $kd_poli,
					'no_reg' => $NoRegNow,
					'kd_pj' => $kd_pj,
					'no_rujukan' => $no_rujukan,
					'no_skdp' => $no_skdp,
					'limit_reg' => 1,
					'waktu_kunjungan' => $waktu_datang,
					'status' => 'Belum'
				);

				$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
				$res = $api->cariByNoRujukan('',$no_rujukan);
				$rw = $res['response']['rujukan'];

				$data['no_rkm_medis'] = $no_rkm_medis; 
				$data['no_rujukan'] = $no_rujukan; 
				$data['tanggal_periksa'] = $tgl_registrasi; 
				$data['nokartu'] = $rw['peserta']['noKartu']; 
				$data['kdDiagnosis'] = $rw['diagnosa']['kode']; 
				$data['nmDiagnosis'] = $rw['diagnosa']['nama']; 
				$data['kdPelayanan'] = $rw['pelayanan']['kode']; 
				$data['nmPelayanan'] = $rw['pelayanan']['nama']; 
				$data['keteranganHakKls'] = $rw['peserta']['hakKelas']['keterangan']; 
				$data['kdHakKls'] = $rw['peserta']['hakKelas']['kode']; 
				$data['keteranganJnsPeserta'] = $rw['peserta']['jenisPeserta']['keterangan']; 
				$data['keteranganstatusPeserta'] = $rw['peserta']['statusPeserta']['keterangan']; 
				$data['nmAsuransiCob'] = $rw['peserta']['cob']['nmAsuransi']; 
				$data['noAsuransiCob'] = $rw['peserta']['cob']['noAsuransi']; 
				$data['tglTATCob'] = $rw['peserta']['cob']['tglTAT']; 
				$data['tglTMTCob'] = $rw['peserta']['cob']['tglTMT']; 
				$data['kdJnsPeserta'] = $rw['peserta']['jenisPeserta']['kode']; 
				$data['kdPoliRujukan'] = $rw['poliRujukan']['kode']; 
				$data['kdProviderPerujuk'] = $rw['provPerujuk']['kode']; 
				$data['nmProviderPerujuk'] = $rw['provPerujuk']['nama']; 
				$data['tglKunjungan'] = $rw['tglKunjungan'];
				$data['keluhan'] = $rw['keluhan'];
				$data['noTelpMr'] = $rw['peserta']['mr']['noTelepon'];

				if($this->Get_model->insert('temp_booking_bpjs',$data,FALSE))
				{
					if($this->Get_model->insert('booking_registrasi',$datas,FALSE))
					{
						$sts = 1;
						$msg = 'Mohon maaf data anda tidak bisa di simpan, telah terjadi kesalahan dalam system kami, silahkan ulangi lagi kemabli.';
					}
					else
					{
						$whRecordBooking 	= array(
							'tanggal' => $tgl_registrasi,
							'kd_dokter' => $kd_dokter,
							'kd_poli' => $kd_poli,
							'subkat' => 'Kuota',
							'kategori' => 'belum'
						);
						$getRecodBooking = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$whRecordBooking);

						$report_log = array();
						if($getRecodBooking[0]->total > 0)
						{
							$report_log['jumlah']	= $getRecodBooking[0]->total + 1;
							$this->Get_model->update('report_pendaftaran_online',$report_log,$whRecordBooking);
						}
						else
						{
							$report_log['tanggal']	= $tgl_registrasi;
							$report_log['kd_dokter']= $kd_dokter;
							$report_log['kd_poli']= $kd_poli;
							$report_log['kategori']	= "belum";
							$report_log['subkat']	= "Kuota";
							$report_log['jumlah']	= 1;
							$this->Get_model->insert('report_pendaftaran_online',$report_log);
						}

						$html = '<table class="table table-striped">';
							$html .= '<tr>';
								$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>No Uru Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$NoRegNow.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nomor Rekam Medik</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$no_rkm_medis.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nama Lengkap</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Periksa</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Waktu Datang</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Poliklinik Tujuan</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Dokter </td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Cara Bayar</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>Jamkesnas</td>';
							$html .= '</tr>';
						$html .= '</table">';

						$sts = 2;
						$msg = $html;
					}
				}
				else
				{
					$sts = 2;
					$msg = 'Gagal ketika menyimpan data.';
				}
				
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}
}
