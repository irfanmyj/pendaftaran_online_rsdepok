<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logviewr extends backend_controller {

	private $logViewer;

	public function __construct(){
		parent::__construct();
		$this->logViewer = new \CILogViewer\CILogViewer();
		$this->load->library(array('user_agent'));
		//$this->site->is_logged_in();
	}

	public function index()
	{
		echo $this->logViewer->showLogs();
    	return;
	}
	
}
