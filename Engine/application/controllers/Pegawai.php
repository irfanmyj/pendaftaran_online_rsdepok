<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		if(hak_akses(a('uri'),a())==FALSE)
		{
			$_SESSION['error'] = 'info';
			$_SESSION['msg']   = 'Mohon maaf anda tidak mempunyai akses pada module ini.';
			redirect(base_url('welc/auth'));
		}
		
		global $Cf;
		$f 		= ($this->uri->segment(3))	? $this->uri->segment(3) : 'f';
		$l 		= ($this->uri->segment(4))	? $this->uri->segment(4) : 10;
		$p 		= ($this->uri->segment(5))	? $this->uri->segment(5) - 1 : 0;

		$pq 	= $p * $l;
		$no 	= ($p * $l) + 1;

		$where 	= '';
		if($f!='f')
		{
			$where = 'nm_lengkap like \'%'.$f.'%\'';
		}

		$jum 	= $this->Get_model->countCustom('pegawai',$where);
		$r 		= $this->Get_model->getRecordList('pegawai','*',$where,'','id_pegawai ASC',$l,$pq);

		//merge data
		$rp		= $this->Get_model->getRecordList('provinsi','id_prov,nama');
		$lsrp	= ConDataToArray($rp,'id_prov','nama');

		$rs		= $this->Get_model->getRecordList('status','id_status,nm_status');
		$lsrs	= ConDataToArray($rs,'id_status','nm_status');

		$rl		= $this->Get_model->getRecordList('level','id_level,nm_level');
		$lsrl	= ConDataToArray($rl,'id_level','nm_level');

		$rj		= $this->Get_model->getRecordList('jabatan','id_jabatan,nm_jabatan');
		$lsrj	= ConDataToArray($rj,'id_jabatan','nm_jabatan');

		$rg		= $this->Get_model->getRecordList('golongan','id_golongan,nm_pangkat,kd_golongan,ruang');
		$lsrg	= ConDataToArray($rg,'id_golongan','nm_pangkat','kd_golongan','ruang');

		$departement 		= $this->Get_model->getRecordList('departement','*','','','id_departement ASC');

		$data = array(
			'title'		=> 'Manajemen pegawai',
			'f' 		=> $f,
			'l' 		=> $l,
			'p' 		=> $p,
			'pq' 		=> $pq,
			'no' 		=> $no,
			'jum' 		=> $jum,
			'r'			=> $r,
			'limit'		=> $Cf->_limit,
			'jk'		=> $Cf->_jk,
			'negara'	=> $Cf->_ng,
			'jk'		=> $Cf->_jk,
			'goldarah'	=> $Cf->_goldarah,
			'provinsi'	=> $lsrp,
			'departement'=> $departement,
			'golongan'	=> $lsrg,
			'jabatan'	=> $lsrj,
			'status'	=> $lsrs,
			'level'		=> $lsrl,
			'inc'		=> 'pegawai'
		);
		$this->site->view('inc',$data);
	}

	public function save()
	{
		$p = $this->input->post();

		$dt['masuk_tgl']		= isset($p['masuk_tgl']) ? date('Y-m-d',strtotime($p['masuk_tgl'])) : '';
		$dt['tgl_lahir']		= isset($p['tgl_lahir']) ? date('Y-m-d',strtotime($p['tgl_lahir'])) : '';
		$dt['nip']				= isset($p['nip']) ? htmlentities($p['nip']) : '';
		$dt['email']			= isset($p['email']) ? htmlentities($p['email']) : '';
		$dt['nm_lengkap']		= isset($p['nm_lengkap']) ? htmlentities($p['nm_lengkap']) : '';
		$dt['tmpt_lahir']		= isset($p['tmpt_lahir']) ? htmlentities($p['tmpt_lahir']) : '';
		$dt['telp']				= isset($p['telp']) ? htmlentities($p['telp']) : '';
		$dt['jk']				= isset($p['jk']) ? htmlentities($p['jk']) : '';
		$dt['gol_darah']		= isset($p['gol_darah']) ? htmlentities($p['gol_darah']) : '';
		$dt['provinsi']			= isset($p['provinsi']) ? htmlentities($p['provinsi']) : '';
		$dt['kabupaten']		= isset($p['kabupaten']) ? htmlentities($p['kabupaten']) : '';
		$dt['kecamatan']		= isset($p['kecamatan']) ? htmlentities($p['kecamatan']) : '';
		$dt['kelurahan']		= isset($p['kelurahan']) ? htmlentities($p['kelurahan']) : '';
		$dt['id_departement']	= isset($p['id_departement']) ? htmlentities($p['id_departement']) : '';
		$dt['id_golongan']		= isset($p['id_golongan']) ? htmlentities($p['id_golongan']) : '';
		$dt['id_status']		= isset($p['id_status']) ? htmlentities($p['id_status']) : '';
		$dt['id_jabatan']		= isset($p['id_jabatan']) ? htmlentities($p['id_jabatan']) : '';

		#$dtuser['password'] 	= isset($p['password']) ? bCrypt($p['password'],12) : '';
		$dtuser['username']		= isset($p['email']) ? htmlentities($p['email']) : '';
		$dtuser['id_level']		= isset($p['id_level']) ? htmlentities($p['id_level']) : '';
		$dtuser['masuk_oleh']	= $this->session->userdata('id_user');
		$dtuser['masuk_tgl']	= date('Y-m-d H:i:s');
		$dtuser['status']		= 'Y';

		if($p['password'])
		{
			$dtuser['password'] = bCrypt($p['password'],12);
		}

		/*echo '<pre>';
		print_r($dtuser);
		exit();*/
		$rules 					= $this->Get_model->pegawai;
		$this->form_validation->set_rules($rules);
		
		if($p['id'])
		{
			if($this->form_validation->run() == FALSE)
			{
				$_SESSION['error'] = '1';
				$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil diperbarui, silakan coba lagi nanti.';
				$this->Get_model->createHistory('Gagal update nama rumpun ilmu dengan nama : '.$p['nm_lengkap'].'',$this->session->userdata('id_user'));
				generateReturn(base_url('pegawai/view'));
			}
			else
			{
				$_SESSION['error'] = '';
				$_SESSION['msg']   = 'Data telah berhasil diperbarui.';
				$this->Get_model->createHistory('Update nama pegawai dengan nama : '.$p['nm_lengkap'].'',$this->session->userdata('id_user'));

				if (!empty($_FILES["image"]["name"])) {
				    $dt['foto'] = $this->_uploadImage($p['nip']);
				} else {
				    $dt['foto'] = $p["old_image"];
				}

				$this->Get_model->update('pegawai',$dt,array('id_pegawai'=>$p['id']));
				$this->Get_model->update('user',$dtuser,array('id_pegawai'=>$p['id']));
				generateReturn(base_url('pegawai/view'));
			}
		}
		else
		{
			if($this->form_validation->run() == FALSE){
				$_SESSION['error'] = '1';
				$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil disimpan, silakan coba lagi nanti.';
				$this->Get_model->createHistory('Gagal tambah nama rumpun ilmu dengan nama : '.$p['nm_lengkap'].'',$this->session->userdata('id_user'));
				generateReturn(base_url('pegawai/view'));
			}
			else
			{
				$_SESSION['error'] = '';
				$_SESSION['msg']   = 'Data telah berhasil simpan.';
				
				$this->Get_model->createHistory('Tambah nama pegawai dengan nama : '.$p['nm_lengkap'].'',$this->session->userdata('id_user'));

				$dt['foto'] = $this->_uploadImage($p['nip']);
				$id = $this->Get_model->insert('pegawai',$dt,FALSE);
				$dtuser['id_pegawai']		= $id;
				$this->Get_model->insert('user',$dtuser,FALSE);
				generateReturn(base_url('pegawai/view'));
			}
		}
	}

	public function delete()
	{
		$uri = $this->uri->segment(3);
		$idl = explode('k', $uri);

		foreach ($idl as $idv)
		{
			$det = $this->Get_model->getRecordList('pegawai','nm_lengkap','id_pegawai=\''.$idv.'\'');
			if($this->Get_model->delete('pegawai',array('id_pegawai'=>$idv)))
			{
				$_SESSION['error'] = '1';
            	$_SESSION['msg']   = 'Data <b> TIDAK </b> berhasil dihapus, silakan coba lagi nanti.';
			}
			else
			{
				$_SESSION['error'] = '';
        		$_SESSION['msg']   = 'Data telah berhasil dihapus';
				$this->Get_model->createHistory('Delete rumpun ilmu dengan nama : '.$det[0]->nm_lengkap.'',$this->session->userdata('ID'));
			}
			//echo $det[0]->nm_level;
		}

		generateReturn(base_url('pegawai/view'));
	}

	private function _uploadImage($file_name='')
	{
	    $config['upload_path']          = './uploads/picture/';
	    $config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG|JPG|PNG|GIF';
	    $config['file_name']            = $file_name;
	    $config['overwrite']			= true;
	    $config['max_size']             = 1024;
	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload('image')) {
	        return $this->upload->data("file_name");
	    }
	    
	    return "default.jpg";
	}

	private function _deleteImage($id)
	{
	    $product = $this->getById($id);
	    if ($product->image != "default.jpg") {
		    $filename = explode(".", $product->image)[0];
			return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
	    }
	}

}
