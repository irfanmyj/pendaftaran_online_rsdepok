<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Get_ajax extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		//$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function getkab()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();
			$get = $this->Get_model->getRecordList('kabupaten','id_kab,nama',array('id_prov'=>$p['id_prov']));
			$ls = ConDataToArray($get,'id_kab','nama');
			echo htmlSelectFromArray($ls, 'name="kabupaten" id="kabupatens" class="form-control input-sm select2" style="width:100%;" ', true);
		}
		else
		{
			echo '';
		}
	}

	public function getkec()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();
			$get = $this->Get_model->getRecordList('kecamatan','id_kec,nama',array('id_kab'=>$p['id_kab']));
			$ls = ConDataToArray($get,'id_kec','nama');
			echo htmlSelectFromArray($ls, 'name="kecamatan" id="kecamatans" class="form-control input-sm select2" style="width:100%;" ', true);
		}
		else
		{
			echo '';
		}
	}

	public function getdesa()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();
			$get = $this->Get_model->getRecordList('kelurahan','id_kel,nama',array('id_kec'=>$p['id_kec']));
			$ls = ConDataToArray($get,'id_kel','nama');
			echo htmlSelectFromArray($ls, 'name="kelurahan" id="kelurahans" class="form-control input-sm select2" style="width:100%;" ', true);
		}
		else
		{
			echo '';
		}
	}

	public function getDetailKamar()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$res = $this->Get_model->getDetailKamar($p['kd']);
			echo '<div class="content no-mb">';
			echo '<table class="table table-border table-responsive-md">';
				echo '<thead>';
					echo '<tr>';
						echo '<th>Nama Ruang</th>';
						echo '<th>Kapasitas</th>';
						echo '<th>Tersedia</th>';
						echo '<th>Tersedia (Laki-laki)</th>';
						echo '<th>Tersedia (Perempuan)</th>';
					echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
					foreach ($res as $k => $v) {
						echo '<tr>';
							echo '<td>'.$v->nm_bangsal.'</td>';
							echo '<td>'.$v->kapasitas.'</td>';
							echo '<td>'.$v->tersedia.'</td>';
							echo '<td>'.$v->tersediapria.'</td>';
							echo '<td>'.$v->tersediawanita.'</td>';
						echo '</tr>';
					}
				echo '</tbody>';
			echo '</table>';
			echo '</div>';
		}
		else
		{
			echo '';
		}
	}

	/*
	BPJS
	*/
	public function getFakses()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Referensi($Cf->_confbpjs_tes);
			$p = $this->input->post();

			$res = $api->faskes(htmlentities($p['nm_daerah'],ENT_QUOTES),htmlentities($p['nm_faskes'],ENT_QUOTES));

			if($res['response']['faskes'])
			{
				echo '<div class="wrap-content b-shadow">';
					echo '<table class="table table-striped">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>No</th>';
								echo '<th>Kode Faskes</th>';
								echo '<th>Nama Faskes</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							$no = 0;
							foreach ($res['response']['faskes'] as $k => $v) {
								$no++;
								echo '<tr>';
									echo '<td>'.$no.'</td>';
									echo '<td>'.$v['kode'].'</td>';
									echo '<td><a href="https://www.google.com/search?q='.str_replace(" ", "+", $v['nama']).'" target="_blank">'.$v['nama'].'</a></td>';
								echo '</tr>';
							}
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
			}else
			{
				echo 'Data tidak ditemukan pada server BPJS.';
			}
		}
		else
		{
			$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
			);
			$this->site->view_error('error_404',$data);
		}
	}

	public function getPelayanan()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Monitoring($Cf->_confbpjs_tes);
			$p = $this->input->post();

			$res = $api->historyPelayanan(htmlentities($p['no_kartu'],ENT_QUOTES),date('Y-m-d',strtotime($p['tgl_awal'])),date('Y-m-d',strtotime($p['tgl_akhir'])));

			if($res['response']['histori'])
			{
				echo '<div class="wrap-content b-shadow">';
					echo '<table class="table table-striped">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>No</th>';
								echo '<th>No Kartu</th>';
								echo '<th>Diagnosa</th>';
								echo '<th>Nama Peserta</th>';
								echo '<th>Kelas Rawat</th>';
								echo '<th>Ppk Pelayanan</th>';
								echo '<th>Tanggal Pulang</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							$no = 0;
							foreach ($res['response']['histori'] as $k => $v) {
								$no++;
								echo '<tr>';
									echo '<td>'.$no.'</td>';
									echo '<td>'.$v['noKartu'].'</td>';
									echo '<td>'.$v['diagnosa'].'</td>';
									echo '<td>'.$v['namaPeserta'].'</td>';
									echo '<td>'.$v['kelasRawat'].'</td>';
									echo '<td>'.$v['ppkPelayanan'].'</td>';
									echo '<td>'.@tanggal_indo($v['tglPlgSep'],true).'</td>';
								echo '</tr>';
							}
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
			}
			else
			{
				echo 'Data tidak ditemukan pada server BPJS.';
			}
		}
		else
		{
			$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
			);
			$this->site->view_error('error_404',$data);
		}
	}

	public function getKepesertaan()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Peserta($Cf->_confbpjs_tes);
			$p = $this->input->post();
			$res = $api->getByNIK(htmlentities($p['no_nik'],ENT_QUOTES),date('Y-m-d',strtotime($p['tgl_pelayanan'])));
			$r = $res['response']['peserta'];
			if($r)
			{
				echo '<div class="wrap-content b-shadow">';
					echo '<table class="table table-striped">';
						echo '<thead>';
							echo '<tr>';
								echo '<th>No Kartu</th>';
								echo '<th>Hak Kelas</th>';
								echo '<th>Nama Peserta</th>';
								echo '<th>Tgl Cetak Kartu</th>';
								echo '<th>Status</th>';
							echo '</tr>';
						echo '</thead>';
						echo '<tbody>';
							echo '<tr>';
								echo '<td>'.$r['noKartu'].'</td>';
								echo '<td>'.$r['hakKelas']['keterangan'].'</td>';
								echo '<td>'.$r['nama'].'</td>';
								echo '<td>'.@tanggal_indo($r['tglCetakKartu'],true).'</td>';
								echo '<td>'.$r['statusPeserta']['keterangan'].'</td>';
							echo '</tr>';
						echo '</tbody>';
					echo '</table>';
				echo '</div>';
			}
			else
			{

			}
		}
	}

	public function getNoRujukan()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
			$p = $this->input->post();
			$no_rujukan = ($p['no_rujukan']) ? $p['no_rujukan'] : '';

			if($no_rujukan)
			{
				$res = $api->cariByNoRujukan('',htmlentities($no_rujukan,ENT_QUOTES));
				if($res['metaData']['code'] == 201 || $res['metaData']['code'] == '' || $res=='')
				{
					$msg = 'Mohon maaf kami tidak menemukan data dengan no rujukan ini.'. $no_rujukan;
				}
				else if($res['response']['rujukan'])
				{
					$rw = $res['response']['rujukan'];
					$_SESSION['no_rujukan'] 	= $no_rujukan;
					$_SESSION['no_skdp'] 		= ($p['no_skdp']) ? $p['no_skdp'] : '';
					$_SESSION['tgl_registrasi'] = $p['tgl_registrasi'];
					$_SESSION['id_poli'] 		= $rw['poliRujukan']['kode'];
					
					$msg = '';
				}
				else
				{
					$msg = 'Mohon maaf koneksi dari bpjs sedang tidak terhubung.';
				}
			}
			else
			{
				$msg = 'Silahkan isi no rujukan anda.';
			}

			$dt  =array(
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function getNoRujukan1()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
			$p = $this->input->post();
			$no_rujukan = ($p['no_rujukan']) ? $p['no_rujukan'] : '';

			if($no_rujukan)
			{
				$res = $api->cariByNoRujukan('RS',htmlentities($no_rujukan,ENT_QUOTES));
				if($res['metaData']['code'] == 201 || $res['metaData']['code'] == '' || $res=='')
				{
					$msg = 'Mohon maaf kami tidak menemukan data dengan no rujukan ini.'. $no_rujukan . $res['metaData']['message'];
				}
				else if($res['response']['rujukan'])
				{
					$rw = $res['response']['rujukan'];
					$_SESSION['no_rujukan'] 	= $no_rujukan;
					$_SESSION['no_skdp'] 		= ($p['no_skdp']) ? $p['no_skdp'] : '';
					$_SESSION['tgl_registrasi'] = $p['tgl_registrasi'];
					$_SESSION['id_poli'] 		= $rw['poliRujukan']['kode'];
					
					$msg = '';
				}
				else
				{
					$msg = 'Mohon maaf koneksi dari bpjs sedang tidak terhubung.';
				}
			}
			else
			{
				$msg = 'Silahkan isi no rujukan anda.';
			}

			$dt  =array(
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function getNoSep()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$api = new Nsulistiyawan\Bpjs\VClaim\Sep($Cf->_confbpjs_tes);
			$p = $this->input->post();
			$no_sep = ($p['no_sep']) ? $p['no_sep'] : '';
			//$no_rujukan = ($p['no_rujukan']) ? $p['no_rujukan'] : '';
			//$kategori = ($p['kategori']) ? $p['kategori'] : '';
			//&& !empty($no_rujukan) && !empty($kategori)
			if(!empty($no_sep))
			{
				$res = $api->cariSEP($no_sep);
				if($res['metaData']['code'] == 201 || $res['metaData']['code'] == '' || $res=='')
				{
					$msg = 'Mohon maaf system tidak menemukan data dengan no sep .'. $no_sep .' ini.';
				}
				else if($res['metaData']['message']=="Sukses")
				{
					$rw = $res['response'];
					$_SESSION['no_sep'] 		= $no_sep;
					//$_SESSION['no_rujukan'] 	= $no_rujukan;
					//$_SESSION['kategori'] 		= $kategori;
					$_SESSION['no_skdp'] 		= ($p['no_skdp']) ? $p['no_skdp'] : '';
					$_SESSION['tgl_registrasi'] = $p['tgl_registrasi'];
					$_SESSION['id_poli'] 		= '';
					
					$msg = '';
				}
				else
				{
					$msg = 'Mohon maaf koneksi dari bpjs sedang tidak terhubung.';
				}
			}
			else
			{
				$msg = 'Ada form yang belum terisi.';
			}

			$dt  =array(
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}	

	/*
	Booking Umum
	*/
	public function umum_step1()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p 				= $this->input->post();
			$tgl_registrasi	= date('Y-m-d',strtotime($p['tgl_registrasi']));
			$dNow 			= date('Y-m-d');
			$tNow 			= date('H:i:s');
			$tgl_kedepan 	= AddTglNext($dNow,$Cf->_hari_daftar,'days');

			$getBooking		= $this->Get_model->getInfoBooking(array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$tgl_registrasi,'a.limit_reg'=>1));

			$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$tgl_registrasi),'','',1);
			
			if($tgl_registrasi == $dNow && $tNow > $Cf->_limit_jam)
			{
				$msg = 'Mohon maaf pendaftaran sudah selesai';
			}
			else if($tgl_registrasi < $dNow)
			{
				$msg = 'Mohon maaf tanggal yang anda pilih kurang dari tanggal saat ini.';
			}
			else if($tgl_registrasi > $tgl_kedepan)
			{
				$msg = 'Mohon maaf tanggal yang anda pilih melebihi batas limit pendaftaran, silahkan mundurkan tanggal kunjungan anda.';
			}
			else if($getBooking[0]->status == 'Belum')
			{
				$msg = 'Anda sudah terdaftar '. tanggal_indo($getBooking[0]->tanggal_periksa,true) . ', pada Dokter '. $getBooking[0]->nm_dokter . ' ke '. $getBooking[0]->nm_poli . ' dengan waktu kunjungan '. tanggal_indo($getBooking[0]->waktu_kunjungan,true);
			}
			else if($getBooking[0]->status == 'Batal')
			{
				$msg = 'Mohon maaf anda tidak bisa mendaftar pada tanggal yang sama, tercatat anda sudah mendaftar pada tanggal sekarang kemudian membatalkan.';
			}
			else if($getLiburNasional)
			{
				$msg = 'Mohon maaf pada '. tanggal_indo($getLiburNasional[0]->tanggal) . ' '. $getLiburNasional[0]->keterangan;
			}
			else
			{
				$msg = '';
				$_SESSION['tgl_registrasi'] = $tgl_registrasi;
			}

			$data = array(
				'msg' => $msg
			);

			echo json_encode($data);
		}
	}

	public function umum_step2()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();

			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = ($this->session->userdata('tgl_registrasi')) ? $this->session->userdata('tgl_registrasi') : $p['tgl_registrasi'];
			$hari_kerja		= hari_indo($tgl_registrasi);
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');
			$tgl_kedepan 	= AddTglNext($dNow,$Cf->_hari_daftar,'days');
			$jumlah_hari	= CountTglNowToNext($dNow,$tgl_kedepan);

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			//array('tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'limit_reg'=>1)
			$where = 'tanggal_periksa=\''.$tgl_registrasi.'\' and kd_poli=\''.$kd_poli.'\' and kd_dokter=\''.$kd_dokter.'\' and limit_reg=1';

			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','COUNT(*) as total',$where);

			$getJumBatal= $this->Get_model->getRecordList('booking_registrasi','COUNT(*) as total',array('tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'limit_reg'=>1,'status'=>'Batal'));
			
			$Getlimit = $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;
			//$total_daftar   =
			$sisikuota		= $limit - ($getJumPendaftar[0]->total - $getJumBatal[0]->total);

			/*echo $getJumPendaftar[0]->total .'<br>';
			echo $limit .'<br>';
			echo $sisikuota .'<br>';
			print_r($p);
			exit();*/
			/*
			Info 	: Query get booking
			Manfaat : untuk cek apakah pasien sudah terdaftar pada tanggal yang dia pilih atau belum, karena dalam satu hari yang sama pasien tidak bisa booking ke dua tempat.
			*/
			$getCekBk		= $this->Get_model->getInfoBooking(array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.tanggal_periksa'=>$tgl_registrasi,'a.kd_poli'=>$kd_poli,'a.kd_dokter'=>$kd_dokter,'a.kd_pj'=>'UMU','a.status'=>'Belum'));

			/*
			Info 	: Query get dokter yang tidak sedang praktik 
			Manfaat	: Untuk memunculkan informasi kepada pasien bahwa dokter sedang tidak praktik.
			*/
			$getDokCuti		= $this->Get_model->getRecordList('dokter_libur','count(*) as total, tanggal',array('kd_dokter'=>$kd_dokter,'tanggal'=>$tgl_registrasi),'','',1);

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));

			if($kd_dokter=='--- Pilih Dokter ---')
			{
				$sts = 1;
				$msg = 'Dokter tujuan anda masih kosong.';
			}
			else if($getDokCuti[0]->total > 0)
			{
				$sts = 1;
				$msg = 'Mohon maaf Dokter : '. $getDokter[0]->nm_dokter .' sedang tidak praktik pada  '. tanggal_indo($getDokCuti[0]->tanggal,true) .'';
			}
			else if($sisikuota == 0 || $sisikuota < 0)
			{
				$sts = 1;
				$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
			}
			else if($getCekBk[0]->tanggal_periksa)
			{
				$sts = 1;
				$msg = 'Mohon maaf anda sudah terdaftar pada : '.tanggal_indo($tgl_registrasi,true).' , Tujuan : '. $getCekBk[0]->nm_poli .' , Dokter : '. $getCekBk[0]->nm_dokter .'';
			}
			else
			{
				$sts = 2;
				$msg = 'Sisa kuota yang tersedia : '.$sisikuota;
			}
			
			$d = array(
				'sts' => $sts,
				'msg' => $msg
			);
			//.' sisa kuota : '. $sisikuota .' -> limit : '. $limit .' - total pendaftara : '. $getJumPendaftar[0]->total .' - jum batal : ' .$getJumBatal[0]->total
			echo json_encode($d);
		}
	}

	public function umum_step3()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = $_SESSION['tgl_registrasi'];
			$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
			$kd_pj 			= 'UMU';
			$hari_kerja		= hari_indo($tgl_registrasi);
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'UMU','limit_reg'=>1,'status'=>'Belum'));
			
			$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
			$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

			// Get No Reg Terbesar Pada tabel booking_registrasi
			$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Get No Reg Terbesar Pada tabel reg_periksa
			$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
			$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
			$conNoReg 	= substr($NoReg, 0, 3);
			$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
			
			// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

           // get Jam Praktek
            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

            // Perkalian limit dan no registrasi
            $menitKedatangan = $limit_kedatangan * $NoRegNow;

            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
			$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

			// Pengurangan 30 menit sebelum praktek
			$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
			date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
			$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

			// Penambahan waktu/menit setiap pasien
			$TglPengurangan = date_create($TglKedatangan);
			date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
			$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

			$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
			$jam_datang = date('H:i:s',strtotime($waktu_datang));
			$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

			if($getJumPendaftar[0]->total >= $limit)
			{
				$sts = 1;
				$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
			}
			else
			{
				$datas = array(
					'tanggal_booking' => $dNow,
					'jam_booking' => $tNow,
					'no_rkm_medis' => $no_rkm_medis,
					'tanggal_periksa' => $tgl_registrasi,
					'kd_dokter' => $kd_dokter,
					'kd_poli' => $kd_poli,
					'no_reg' => $NoRegNow,
					'kd_pj' => $kd_pj,
					'limit_reg' => 1,
					'waktu_kunjungan' => $waktu_datang,
					'status' => 'Belum'
				);

				if($this->Get_model->insert('booking_registrasi',$datas,FALSE))
				{
					$sts = 1;
					$msg = 'Mohon maaf data anda tidak bisa di simpan, telah terjadi kesalahan dalam system kami, silahkan ulangi lagi kemabli.';
				}
				else
				{
					$whRecordBooking 	= array(
						'tanggal' => $tgl_registrasi,
						'kd_dokter' => $kd_dokter,
						'kd_poli' => $kd_poli,
						'subkat' => 'Online',
						'kategori' => 'belum'
					);
					$getRecodBooking = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$whRecordBooking);

					$report_log = array();
					if($getRecodBooking[0]->total > 0)
					{
						$report_log['jumlah']	= $getRecodBooking[0]->total + 1;
						$this->Get_model->update('report_pendaftaran_online',$report_log,$whRecordBooking);
					}
					else
					{
						$report_log['tanggal']	= $tgl_registrasi;
						$report_log['kd_dokter']= $kd_dokter;
						$report_log['kategori']	= "belum";
						$report_log['subkat']	= "Online";
						$report_log['jumlah']	= 1;
						$this->Get_model->insert('report_pendaftaran_online',$report_log);
					}

					$html = '<table class="table table-striped">';
						$html .= '<tr>';
							$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>No Uru Booking</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$NoRegNow.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Nomor Rekam Medik</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$no_rkm_medis.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Nama Lengkap</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Tanggal Booking</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Tanggal Periksa</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Waktu Datang</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Poliklinik Tujuan</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Dokter </td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Cara Bayar</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>Umum</td>';
						$html .= '</tr>';
					$html .= '</table">';

					$sts = 2;
					$msg = $html;
				}
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function bpjs_step3()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = ($_SESSION['tgl_registrasi']) ? $_SESSION['tgl_registrasi'] : '';
			$no_rujukan 	= ($_SESSION['no_rujukan']) ? $_SESSION['no_rujukan'] : '';
			$no_skdp 		= ($_SESSION['no_skdp']) ? $_SESSION['no_skdp'] : '000001';
			$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
			$kd_pj 			= 'BPJ';
			$hari_kerja		= hari_indo($tgl_registrasi);
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'BPJ','limit_reg'=>1,'status'=>'Belum'));
			
			$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
			$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

			// Get No Reg Terbesar Pada tabel booking_registrasi
			$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Get No Reg Terbesar Pada tabel reg_periksa
			$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
			$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
			$conNoReg 	= substr($NoReg, 0, 3);
			$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
			
			// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

           // get Jam Praktek
            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

            // Perkalian limit dan no registrasi
            $menitKedatangan = $limit_kedatangan * $NoRegNow;

            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
			$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

			// Pengurangan 30 menit sebelum praktek
			$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
			date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
			$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

			// Penambahan waktu/menit setiap pasien
			$TglPengurangan = date_create($TglKedatangan);
			date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
			$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

			$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
			$jam_datang = date('H:i:s',strtotime($waktu_datang));
			$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

			if($getJumPendaftar[0]->total >= $limit)
			{
				$sts = 1;
				$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
			}
			else
			{
				$datas = array(
					'tanggal_booking' => $dNow,
					'jam_booking' => $tNow,
					'no_rkm_medis' => $no_rkm_medis,
					'tanggal_periksa' => $tgl_registrasi,
					'kd_dokter' => $kd_dokter,
					'kd_poli' => $kd_poli,
					'no_reg' => $NoRegNow,
					'kd_pj' => $kd_pj,
					'no_rujukan' => $no_rujukan,
					'no_skdp' => $no_skdp,
					'limit_reg' => 1,
					'waktu_kunjungan' => $waktu_datang,
					'status' => 'Belum'
				);

				$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
				$res = $api->cariByNoRujukan('',$no_rujukan);
				$rw = $res['response']['rujukan'];

				$data['no_rkm_medis'] = $no_rkm_medis; 
				$data['no_rujukan'] = $no_rujukan; 
				$data['tanggal_periksa'] = $tgl_registrasi; 
				$data['nokartu'] = $rw['peserta']['noKartu']; 
				$data['kdDiagnosis'] = $rw['diagnosa']['kode']; 
				$data['nmDiagnosis'] = $rw['diagnosa']['nama']; 
				$data['kdPelayanan'] = $rw['pelayanan']['kode']; 
				$data['nmPelayanan'] = $rw['pelayanan']['nama']; 
				$data['keteranganHakKls'] = $rw['peserta']['hakKelas']['keterangan']; 
				$data['kdHakKls'] = $rw['peserta']['hakKelas']['kode']; 
				$data['keteranganJnsPeserta'] = $rw['peserta']['jenisPeserta']['keterangan']; 
				$data['keteranganstatusPeserta'] = $rw['peserta']['statusPeserta']['keterangan']; 
				$data['nmAsuransiCob'] = $rw['peserta']['cob']['nmAsuransi']; 
				$data['noAsuransiCob'] = $rw['peserta']['cob']['noAsuransi']; 
				$data['tglTATCob'] = $rw['peserta']['cob']['tglTAT']; 
				$data['tglTMTCob'] = $rw['peserta']['cob']['tglTMT']; 
				$data['kdJnsPeserta'] = $rw['peserta']['jenisPeserta']['kode']; 
				$data['kdPoliRujukan'] = $rw['poliRujukan']['kode']; 
				$data['kdProviderPerujuk'] = $rw['provPerujuk']['kode']; 
				$data['nmProviderPerujuk'] = $rw['provPerujuk']['nama']; 
				$data['tglKunjungan'] = $rw['tglKunjungan'];
				$data['keluhan'] = $rw['keluhan'];
				$data['noTelpMr'] = $rw['peserta']['mr']['noTelepon'];

				if($this->Get_model->insert('temp_booking_bpjs',$data,FALSE))
				{
					if($this->Get_model->insert('booking_registrasi',$datas,FALSE))
					{
						$sts = 1;
						$msg = 'Mohon maaf data anda tidak bisa di simpan, telah terjadi kesalahan dalam system kami, silahkan ulangi lagi kemabli.';
					}
					else
					{
						$whRecordBooking 	= array(
							'tanggal' => $tgl_registrasi,
							'kd_dokter' => $kd_dokter,
							'kd_poli' => $kd_poli,
							'subkat' => 'Online',
							'kategori' => 'belum'
						);
						$getRecodBooking = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$whRecordBooking);

						$report_log = array();
						if($getRecodBooking[0]->total > 0)
						{
							$report_log['jumlah']	= $getRecodBooking[0]->total + 1;
							$this->Get_model->update('report_pendaftaran_online',$report_log,$whRecordBooking);
						}
						else
						{
							$report_log['tanggal']	= $tgl_registrasi;
							$report_log['kd_dokter']= $kd_dokter;
							$report_log['kategori']	= "belum";
							$report_log['subkat']	= "Online";
							$report_log['jumlah']	= 1;
							$this->Get_model->insert('report_pendaftaran_online',$report_log);
						}

						$html = '<table class="table table-striped">';
							$html .= '<tr>';
								$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>No Uru Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$NoRegNow.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nomor Rekam Medik</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$no_rkm_medis.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nama Lengkap</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Periksa</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Waktu Datang</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Poliklinik Tujuan</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Dokter </td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Cara Bayar</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>Jamkesnas</td>';
							$html .= '</tr>';
						$html .= '</table">';

						$sts = 2;
						$msg = $html;
					}
				}
				else
				{
					$sts = 2;
					$msg = 'Gagal ketika menyimpan data.';
				}
				
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function bpjs_stepri3()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			global $Cf;
			$p = $this->input->post();
			$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
			$kd_dokter 		= $p['kd_dokter'];
			$kd_poli 		= $p['kd_poli'];
			$tgl_registrasi = ($_SESSION['tgl_registrasi']) ? $_SESSION['tgl_registrasi'] : '';
			$no_rujukan 	= ($_SESSION['no_rujukan']) ? $_SESSION['no_rujukan'] : '';
			$no_sep 		= ($_SESSION['no_sep']) ? $_SESSION['no_sep'] : '';
			$no_skdp 		= ($_SESSION['no_skdp']) ? $_SESSION['no_skdp'] : '000001';
			$kategori 		= ($_SESSION['kategori']) ? $_SESSION['kategori'] : '';
			$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
			$kd_pj 			= 'BPJ';
			$hari_kerja		= hari_indo($tgl_registrasi);
			$dNow			= date('Y-m-d');
			$tNow 			= date('H:i:s');

			/*
			Info 	: Query get jumlah pendaftar
			Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
			*/
			$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'BPJ','limit_reg'=>1,'status'=>'Belum'));
			
			$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
			$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

			/*
			Info 	: Query get dokter
			*/
			$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
			$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
			$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

			// Get No Reg Terbesar Pada tabel booking_registrasi
			$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'status'=>'Belum'));

			// Get No Reg Terbesar Pada tabel reg_periksa
			$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

			// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
			$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
			$conNoReg 	= substr($NoReg, 0, 3);
			$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
			
			// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

           // get Jam Praktek
            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

            // Perkalian limit dan no registrasi
            $menitKedatangan = $limit_kedatangan * $NoRegNow;

            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
			$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

			// Pengurangan 30 menit sebelum praktek
			$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
			date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
			$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

			// Penambahan waktu/menit setiap pasien
			$TglPengurangan = date_create($TglKedatangan);
			date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
			$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

			$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
			$jam_datang = date('H:i:s',strtotime($waktu_datang));
			$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

			if($getJumPendaftar[0]->total >= $limit)
			{
				$sts = 1;
				$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
			}
			else
			{
				$datas = array(
					'tanggal_booking' => $dNow,
					'jam_booking' => $tNow,
					'no_rkm_medis' => $no_rkm_medis,
					'tanggal_periksa' => $tgl_registrasi,
					'kd_dokter' => $kd_dokter,
					'kd_poli' => $kd_poli,
					'no_reg' => $NoRegNow,
					'kd_pj' => $kd_pj,
					'no_rujukan' => $no_rujukan,
					'no_sep' => $no_sep,
					'no_skdp' => $no_skdp,
					'limit_reg' => 1,
					'waktu_kunjungan' => $waktu_datang,
					'status' => 'Belum'
				);


				if($kategori == 'nokartu')
				{
					$api = new Nsulistiyawan\Bpjs\VClaim\Peserta($Cf->_confbpjs_tes);
					$res = $api->getByNoKartu($no_rujukan,date('Y-m-d'));
					$rw = $res['response']['peserta'];

					$data['no_rkm_medis'] = $no_rkm_medis; 
					$data['no_rujukan'] = $no_rujukan; 
					$data['no_sep'] = $no_sep; 
					$data['tanggal_periksa'] = $tgl_registrasi; 
					$data['nokartu'] = $rw['noKartu']; 
					$data['kdDiagnosis'] = ''; 
					$data['nmDiagnosis'] = ''; 
					$data['kdPelayanan'] = ''; 
					$data['nmPelayanan'] = ''; 
					$data['keteranganHakKls'] = $rw['hakKelas']['keterangan']; 
					$data['kdHakKls'] = $rw['hakKelas']['kode']; 
					$data['keteranganJnsPeserta'] = $rw['jenisPeserta']['keterangan']; 
					$data['keteranganstatusPeserta'] = $rw['statusPeserta']['keterangan'];
					$data['nmAsuransiCob'] = $rw['cob']['nmAsuransi']; 
					$data['noAsuransiCob'] = $rw['cob']['noAsuransi']; 
					$data['tglTATCob'] = $rw['cob']['tglTAT']; 
					$data['tglTMTCob'] = $rw['cob']['tglTMT']; 
					$data['kdJnsPeserta'] = $rw['jenisPeserta']['keterangan']; 
					$data['kdPoliRujukan'] = $kd_poli; 
					$data['kdProviderPerujuk'] = $rw['provUmum']['kdProvider']; 
					$data['nmProviderPerujuk'] = $rw['provUmum']['nmProvider']; 
					$data['tglKunjungan'] = '';
					$data['keluhan'] = '';
					$data['noTelpMr'] = $rw['mr']['noTelepon'];
				}
				else if($kategori == 'norujukan')
				{
					$api = new Nsulistiyawan\Bpjs\VClaim\Rujukan($Cf->_confbpjs_tes);
					$res = $api->cariByNoRujukan('',$no_rujukan);
					$rw = $res['response']['rujukan'];

					$data['no_rkm_medis'] = $no_rkm_medis; 
					$data['no_rujukan'] = $no_rujukan; 
					$data['no_sep'] = $no_sep; 
					$data['tanggal_periksa'] = $tgl_registrasi; 
					$data['nokartu'] = $rw['peserta']['noKartu']; 
					$data['kdDiagnosis'] = $rw['diagnosa']['kode']; 
					$data['nmDiagnosis'] = $rw['diagnosa']['nama']; 
					$data['kdPelayanan'] = $rw['pelayanan']['kode']; 
					$data['nmPelayanan'] = $rw['pelayanan']['nama']; 
					$data['keteranganHakKls'] = $rw['peserta']['hakKelas']['keterangan']; 
					$data['kdHakKls'] = $rw['peserta']['hakKelas']['kode']; 
					$data['keteranganJnsPeserta'] = $rw['peserta']['jenisPeserta']['keterangan']; 
					$data['keteranganstatusPeserta'] = $rw['peserta']['statusPeserta']['keterangan']; 
					$data['nmAsuransiCob'] = $rw['peserta']['cob']['nmAsuransi']; 
					$data['noAsuransiCob'] = $rw['peserta']['cob']['noAsuransi']; 
					$data['tglTATCob'] = $rw['peserta']['cob']['tglTAT']; 
					$data['tglTMTCob'] = $rw['peserta']['cob']['tglTMT']; 
					$data['kdJnsPeserta'] = $rw['peserta']['jenisPeserta']['kode']; 
					$data['kdPoliRujukan'] = $rw['poliRujukan']['kode']; 
					$data['kdProviderPerujuk'] = $rw['provPerujuk']['kode']; 
					$data['nmProviderPerujuk'] = $rw['provPerujuk']['nama']; 
					$data['tglKunjungan'] = $rw['tglKunjungan'];
					$data['keluhan'] = $rw['keluhan'];
					$data['noTelpMr'] = $rw['peserta']['mr']['noTelepon'];
				}

				if($this->Get_model->insert('temp_booking_bpjs',$data,FALSE))
				{
					if($this->Get_model->insert('booking_registrasi',$datas,FALSE))
					{
						$sts = 1;
						$msg = 'Mohon maaf data anda tidak bisa di simpan, telah terjadi kesalahan dalam system kami, silahkan ulangi lagi kemabli.';
					}
					else
					{
						$whRecordBooking 	= array(
							'tanggal' => $tgl_registrasi,
							'kd_dokter' => $kd_dokter,
							'kd_poli' => $kd_poli,
							'subkat' => 'Online',
							'kategori' => 'belum'
						);
						$getRecodBooking = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$whRecordBooking);

						$report_log = array();
						if($getRecodBooking[0]->total > 0)
						{
							$report_log['jumlah']	= $getRecodBooking[0]->total + 1;
							$this->Get_model->update('report_pendaftaran_online',$report_log,$whRecordBooking);
						}
						else
						{
							$report_log['tanggal']	= $tgl_registrasi;
							$report_log['kd_dokter']= $kd_dokter;
							$report_log['kategori']	= "belum";
							$report_log['subkat']	= "Online";
							$report_log['jumlah']	= 1;
							$this->Get_model->insert('report_pendaftaran_online',$report_log);
						}

						$html = '<table class="table table-striped">';
							$html .= '<tr>';
								$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>No Uru Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$NoRegNow.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nomor Rekam Medik</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$no_rkm_medis.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Nama Lengkap</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Booking</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Tanggal Periksa</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Waktu Datang</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Poliklinik Tujuan</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Dokter </td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Cara Bayar</td>';
								$html .= '<td width="10">:</td>';
								$html .= '<td>Jamkesnas</td>';
							$html .= '</tr>';
						$html .= '</table">';

						$sts = 2;
						$msg = $html;
					}
				}
				else
				{
					$sts = 2;
					$msg = 'Gagal ketika menyimpan data.';
				}
				
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
	}

	public function batal()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();

			if($p['kd_poli']!='' && $p['kd_dokter']!='' && $p['tanggal_periksa']!='')
			{
				$where = array(
						'kd_poli'=>$p['kd_poli'],
						'kd_dokter'=>$p['kd_dokter'],
						'tanggal_periksa'=>$p['tanggal_periksa'],
						'no_rkm_medis'=>$this->session->userdata('no_rkm_medis')
					);

				$where2 = array(
						'kd_poli'=>$p['kd_poli'],
						'kd_dokter'=>$p['kd_dokter'],
						'tanggal'=>$p['tanggal_periksa'],
						'subkat' => 'Online',
						'kategori' => 'batal'
					);

				$report_log = array();
				$data = array('status'=>'Batal');

				$coun_batal = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$where2);

				if($this->Get_model->update('booking_registrasi',$data,$where)){

					if($coun_batal[0]->total > 0)
					{
						$report_log['jumlah']	= $coun_batal[0]->total + 1;
						$this->Get_model->update('report_pendaftaran_online',$report_log,$where2);
					}
					else
					{
						$report_log['tanggal']	= $p['tanggal_periksa'];
						$report_log['kd_poli']	= $p['kd_poli'];
						$report_log['kd_dokter']	= $p['kd_dokter'];
						$report_log['kategori']	= "batal";
						$report_log['subkat']	= 'Online';
						$report_log['jumlah']	= 1;
						$this->Get_model->insert('report_pendaftaran_online',$report_log);
					}

					$sts = 1;
					$msg = '<h3><i class="fa fa-check-circle-o"></i> Pembatalan berhasil.</h3>';
				}
				else
				{
					$sts = 2;
					$msg = '<h3><i class="fa fa-times-circle-o"></i> Pembatalan gagal.</h3>';
				}
			}
			else
			{
				$sts = 2;
				$msg = '<h3><i class="fa fa-times-circle-o"></i> Mohon maaf permintaan anda tidak bisa di proses.</h3>';
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
		else
		{
			echo '';
		}
	}

	public function batalbpjs()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();

			if($p['kd_poli']!='' && $p['kd_dokter']!='' && $p['tanggal_periksa']!='')
			{
				$where = array(
						'kd_poli'=>$p['kd_poli'],
						'kd_dokter'=>$p['kd_dokter'],
						'tanggal_periksa'=>$p['tanggal_periksa'],
						'no_rkm_medis'=>$this->session->userdata('no_rkm_medis')
					);

				$where2 = array(
						'kd_poli'=>$p['kd_poli'],
						'kd_dokter'=>$p['kd_dokter'],
						'tanggal'=>$p['tanggal_periksa'],
						'subkat' => 'Online',
						'kategori' => 'batal'
					);

				$report_log = array();
				$data = array('status'=>'Batal');

				$coun_batal = $this->Get_model->getRecordList('report_pendaftaran_online','count(*) as total',$where2);

				if($this->Get_model->update('booking_registrasi',$data,$where)){
					$this->Get_model->delete('temp_booking_bpjs',$where);
					if($coun_batal[0]->total > 0)
					{
						$report_log['jumlah']	= $coun_batal[0]->total + 1;
						$this->Get_model->update('report_pendaftaran_online',$report_log,$where2);
					}
					else
					{
						$report_log['tanggal']	= $p['tanggal_periksa'];
						$report_log['kd_poli']	= $p['kd_poli'];
						$report_log['kd_dokter']	= $p['kd_dokter'];
						$report_log['kategori']	= "batal";
						$report_log['subkat']	= 'Online';
						$report_log['jumlah']	= 1;
						$this->Get_model->insert('report_pendaftaran_online',$report_log);
					}

					$sts = 1;
					$msg = '<h3><i class="fa fa-check-circle-o"></i> Pembatalan berhasil.</h3>';
				}
				else
				{
					$sts = 2;
					$msg = '<h3><i class="fa fa-times-circle-o"></i> Pembatalan gagal.</h3>';
				}
			}
			else
			{
				$sts = 2;
				$msg = '<h3><i class="fa fa-times-circle-o"></i> Mohon maaf permintaan anda tidak bisa di proses.</h3>';
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
		else
		{
			echo '';
		}
	}

	public function gantipoli2()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();

			if($p['kd_poli']!='' && $p['kd_dokter']!='' && $p['tgl_registrasi']!='')
			{
				$no_rkm_medis	= $this->session->userdata('no_rkm_medis');
				$kd_dokter 		= $p['kd_dokter'];
				$kd_poli 		= $p['kd_poli'];
				$tgl_registrasi = $p['tgl_registrasi'];
				$tgl_norawat	= date('Y/m/d',strtotime($tgl_registrasi));
				$kd_pj 			= 'UMU';
				$hari_kerja		= hari_indo($tgl_registrasi);
				$dNow			= date('Y-m-d');
				$tNow 			= date('H:i:s');

				/*
				Info 	: Query get jumlah pendaftar
				Manfaat	: Untuk membandingakn jumlah pendaftar dengan kuota, sehingga mendaptkan jumlah sisa kuota
				*/
				$getJumPendaftar= $this->Get_model->getRecordList('booking_registrasi','count(limit_reg) as total',array('no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'tanggal_periksa'=>$tgl_registrasi,'kd_poli'=>$kd_poli,'kd_dokter'=>$kd_dokter,'kd_pj'=>'UMU','limit_reg'=>1,'status'=>'Belum'));
				
				$Getlimit 			= $this->Get_model->getRecordList('limit_pasien_online','*',array('kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'hari_kerja'=>$hari_kerja));
				$limit 			= (@$Getlimit[0]->limit_reg) ? $Getlimit[0]->limit_reg : 0;

				/*
				Info 	: Query get dokter
				*/
				$getDokter 		= $this->Get_model->getRecordList('dokter','kd_dokter,nm_dokter',array('kd_dokter'=>$kd_dokter));
				$getPoli 		= $this->Get_model->getRecordList('poliklinik','kd_poli,nm_poli',array('kd_poli'=>$kd_poli));
				$getPasien = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$no_rkm_medis),'','',1,'');

				// Get No Reg Terbesar Pada tabel booking_registrasi
				$getNoReg1	= $this->Get_model->getRecordList('booking_registrasi','max(no_reg) as no_reg',array('tanggal_periksa'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli,'status'=>'Belum'));

				// Get No Reg Terbesar Pada tabel reg_periksa
				$getNoReg2	= $this->Get_model->getRecordList('reg_periksa','max(no_reg) as no_reg',array('tgl_registrasi'=>$tgl_registrasi,'kd_dokter'=>$kd_dokter,'kd_poli'=>$kd_poli));

				// Mengambil nomor registrasi yang paling terbesar dari kedua tabel reg_periksa dan booking_registrasi
				$NoReg 		= getNoReg($getNoReg1[0]->no_reg,$getNoReg2[0]->no_reg);
				$conNoReg 	= substr($NoReg, 0, 3);
				$NoRegNow	= sprintf('%03s', ($conNoReg + 1));
				
				// Ambil waktu pengurangan jam praktek dan waktu praktek dari tabel limit_pasien_online
	            $getLimitDokter		= $this->Get_model->getRecordList('limit_pasien_online','limit_kedatangan, waktu_praktek',array('kd_dokter'=>$kd_dokter),'','',1); 
	            $limit_kedatangan 	= $getLimitDokter[0]->limit_kedatangan;
	            $waktu_praktek 		= $getLimitDokter[0]->waktu_praktek;

	           // get Jam Praktek
	            $getJamPraktek		= $this->Get_model->getRecordList('jadwal','jam_mulai',array('kd_dokter'=>$kd_dokter,'hari_kerja'=>$hari_kerja));

	            // Perkalian limit dan no registrasi
	            $menitKedatangan = $limit_kedatangan * $NoRegNow;

	            // Menyatukan Tanggal Registrasi dengan Jam mulai prkatek
				$MergeTanggal = date('Y-m-d H:i:s', strtotime($tgl_registrasi . $getJamPraktek[0]->jam_mulai));

				// Pengurangan 30 menit sebelum praktek
				$ConTglKedatangan = date_create($MergeTanggal); // Jadikan format data menjadi tanggal
				date_add($ConTglKedatangan, date_interval_create_from_date_string('-'. $waktu_praktek .' minutes'));
				$TglKedatangan =  date_format($ConTglKedatangan, 'Y-m-d H:i:s');

				// Penambahan waktu/menit setiap pasien
				$TglPengurangan = date_create($TglKedatangan);
				date_add($TglPengurangan, date_interval_create_from_date_string('+'. $menitKedatangan . ' minutes'));
				$waktu_datang = date_format($TglPengurangan, 'Y-m-d H:i:s');

				$tanggal_datang = date('Y-m-d',strtotime($waktu_datang));
				$jam_datang = date('H:i:s',strtotime($waktu_datang));
				$waktu_format_indonesia = tanggal_indo($tanggal_datang,true);

				if($getJumPendaftar[0]->total >= $limit)
				{
					$sts = 2;
					$msg = 'Mohon maaf untuk Dokter : '.@$getDokter[0]->nm_dokter.' pada Poli : '. $getPoli[0]->nm_poli.' sudah penuh.';
				}
				else
				{
					$where = array(
						'kd_poli'=>$p['poli_awal'],
						'kd_dokter'=>$p['dokter_awal'],
						'tanggal_periksa'=>$p['tgl_registrasi'],
						'no_rkm_medis'=>$this->session->userdata('no_rkm_medis')
					);

					$data = array(
						'tanggal_periksa' => $tgl_registrasi,
						'kd_dokter' => $kd_dokter,
						'kd_poli' => $kd_poli,
						'no_reg' => $NoRegNow,
						'kd_pj' => $kd_pj,
						'waktu_kunjungan' => $waktu_datang,
						'status' => 'Belum'
					);

					$html = '<table class="table table-striped">';
						$html .= '<tr>';
							$html .= '<td colspan="3"><i class="fa fa-check"></i> Selamat pendaftaran anda berhasil</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>No Uru Booking</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$NoRegNow.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Nomor Rekam Medik</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$no_rkm_medis.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Nama Lengkap</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$this->session->userdata('nm_lengkap').'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Tanggal Booking</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($dNow,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Tanggal Periksa</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($tgl_registrasi,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Waktu Datang</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.tanggal_indo($waktu_datang,true).'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Poliklinik Tujuan</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$getPoli[0]->nm_poli.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Dokter </td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>'.$getDokter[0]->nm_dokter.'</td>';
						$html .= '</tr>';
						$html .= '<tr>';
							$html .= '<td>Cara Bayar</td>';
							$html .= '<td width="10">:</td>';
							$html .= '<td>Umum</td>';
						$html .= '</tr>';
					$html .= '</table">';

					if($this->Get_model->update('booking_registrasi',$data,$where)){
						// Update Aktivitas
						$this->Get_model->createHistory('Berhasil, anda '.$this->session->userdata('nm_lengkap').' telah melakukan Perubahan poli, dokter pada '.date('Y-m-d H:i:s'),$this->session->userdata('no_rkm_medis'));
						$sts = 1;
						$msg = $html;
					}
					else
					{
						$this->Get_model->createHistory('Gagal, anda '.$this->session->userdata('nm_lengkap').' telah melakukan Perubahan poli, dokter pada '.date('Y-m-d H:i:s'),$this->session->userdata('no_rkm_medis'));
						$sts = 2;
						$msg = '<h3><i class="fa fa-times-circle-o"></i> Perubahan gagal.</h3>';
					}

				}
			}
			else
			{
				$sts = 2;
				$msg = '<h3><i class="fa fa-times-circle-o"></i> Mohon maaf permintaan anda tidak bisa di proses.</h3>';
			}

			$dt = array(
				'sts' => $sts,
				'msg' => $msg
			);

			echo json_encode($dt);
		}
		else
		{
			echo '';
		}
	}

	/*
	Validasi Booking Umum
	*/
	public function getLiburNasional()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p 				= $this->input->post();
				$tgl_registrasi	= date('Y-m-d',strtotime($p['tgl_registrasi']));
			$getLiburNasional= $this->Get_model->getRecordList('libur_nasional','',array('tanggal'=>$tgl_registrasi),'','',1);

			if($getLiburNasional)
			{
				$msg = 'Mohon maaf pada '. tanggal_indo($getLiburNasional[0]->tanggal) . ' '. $getLiburNasional[0]->keterangan;
			}
			else
			{
				$msg = '';
			}

			$data = array(
				'msg' => $msg
			);

			echo json_encode($data);
		}
	}

	public function getDokter()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();
			$res = $this->Get_model->getDokterPoli(array('b.kd_poli'=>$p['kd_poli'],'b.hari_kerja'=>$p['hari']));
			/*print_r($res);
			exit();*/
			if($res)
			{
				echo '<select name="kd_dokter" id="kd_dokter" class="form-control show-tick">';
					echo '<option>--- Pilih Dokter ---</option>';
					foreach ($res as $k => $v) {
						echo '<option value='.$v->kd_dokter.'>'.$v->nm_dokter.'</option>';
					}
				echo '</select>';
			}
			else
			{
				echo '<select name="kd_dokter" id="kd_dokter" class="form-control show-tick">';
					echo '<option>--- Pilih Dokter ---</option>';
				echo '</select>';
			}
		}
	}

	public function getRiwayatPemeriksaan()
	{
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			$p = $this->input->post();
			$where = array('a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),'a.no_rawat'=>$p['no_rawat']);

			$r1 = $this->Get_model->GetRiwayatPemeriksaanRjDr($where);
			$r2 = $this->Get_model->GetRiwayatPemeriksaanRjPr($where);
			$r3 = $this->Get_model->GetRiwayatPemeriksaanObat($where);

			$html = '<div class="wrap-title">';
				$html .= '<h3 class="text-primary">INFORMASI PRIBADI</h3>';
			$html .= '<div>';
			$html .= '<table class="table table-striped table-border table-responsive">';
				$html .= '<tr>';
					$html .= '<td>Nama Pasien</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td>'.$p['nm_pasien'].'</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>Nama Dokter</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td>'.$p['nm_dokter'].'</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>Nama Poliklinik</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td>'.$p['nm_poli'].'</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>Cara Bayar</td>';
					$html .= '<td width="10">:</td>';
					$html .= '<td>'.$p['png_jawab'].'</td>';
				$html .= '</tr>';
			$html .= '</table>';

			$html .= '<div class="wrap-title">';
				$html .= '<h3 class="text-primary">RIWAYAT PEMERIKSAAN DOKTER</h3>';
			$html .= '<div>';
			$html .= '<table class="table table-striped table-border table-responsive">';
				$html .= '<tr>';
					$html .= '<td>Kode Jenis Perawatan</td>';
					$html .= '<td>Kategori Perawatan</td>';
					$html .= '<td>Nama Perawatan</td>';
				$html .= '</tr>';
				if($r1)
				{
					foreach ($r1 as $k => $v) {
						$html .= '<tr>';
							$html .= '<td>'.$v->kd_jenis_prw.'</td>';
							$html .= '<td>'.$v->nm_kategori.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
						$html .= '</tr>';
					}
				}
			$html .= '</table>';
			$html .= '<br>';

			$html .= '<div class="wrap-title">';
				$html .= '<h3 class="text-primary">RIWAYAT PEMERIKSAAN PETUGAS</h3>';
			$html .= '<div>';
			$html .= '<table class="table table-striped table-border table-responsive">';
				$html .= '<tr>';
					$html .= '<td>Kode Jenis Perawatan</td>';
					$html .= '<td>Kategori Perawatan</td>';
					$html .= '<td>Nama Perawatan</td>';
				$html .= '</tr>';
				if($r2)
				{
					foreach ($r2 as $k => $v) {
						$html .= '<tr>';
							$html .= '<td>'.$v->kd_jenis_prw.'</td>';
							$html .= '<td>'.$v->nm_kategori.'</td>';
							$html .= '<td>'.$v->nm_perawatan.'</td>';
						$html .= '</tr>';
					}
				}	
			$html .= '</table>';

			$html .= '<br>';
			$html .= '<div class="wrap-title">';
				$html .= '<h3 class="text-primary">RIWAYAT PEMBERIAN OBAT</h3>';
			$html .= '<div>';
			$html .= '<table class="table table-striped table-border table-responsive">';
				$html .= '<tr>';
					$html .= '<td>Kode Barang</td>';
					$html .= '<td>Nama Barang</td>';
					$html .= '<td>Jumlah</td>';
					$html .= '<td>Satuan</td>';
				$html .= '</tr>';
				if($r3)
				{
					foreach ($r3 as $k => $v) {
						$html .= '<tr>';
							$html .= '<td>'.$v->kode_brng.'</td>';
							$html .= '<td>'.$v->nama_brng.'</td>';
							$html .= '<td>'.$v->jml.'</td>';
							$html .= '<td>'.$v->satuan.'</td>';
						$html .= '</tr>';
					}
				}	
			$html .= '</table>';

			echo $html;
		}
		else
		{
			echo '';
		}
	}

}
