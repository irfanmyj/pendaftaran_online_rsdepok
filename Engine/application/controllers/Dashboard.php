<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		global $Cf;
		unset($_SESSION['tgl_registrasi']);
		unset($_SESSION['id_poli']);
		unset($_SESSION['id_poli1']);
		unset($_SESSION['id_poli2']);
		unset($_SESSION['no_rujukan']);
		unset($_SESSION['no_skdp']);

		$where = array(
			'd.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),
			'a.status'=>'Y',
			'd.status'=>'Dokter Berhalangan'
		);

		$res = $this->Get_model->GetNotifikasi($where);
		
		$data = array(
			'title'		=> 'Manajemen Bahasa',
			'res'		=> $res,
			'inc'		=> 'home'
		);
		

		//print_r($this->session->userdata('usia'));
		//exit();
		$this->site->view('inc',$data);
	}
}
