<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bookinglansia extends backend_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->site->is_logged_in();
	}

	public function index()
	{
		$data = array(
			'heading' => 'Akses salah...',
			'message' => 'Maaf kami tidak bisa memunculkan halaman yang anda cari..'
		);
		$this->site->view_error('error_404',$data);
	}

	public function view()
	{
		unset($_SESSION['tgl_registrasi']);
		$data = array(
			'title'		=> 'Halaman Booking',
			'inc'		=> 'lansiabooking'
		);
		$this->site->view('inc',$data);
	}

	public function umum()
	{
		unset($_SESSION['tgl_registrasi']);
		$data = array(
			'title'		=> 'Booking Umum',
			'inc'		=> 'lansiabumum'
		);
		$this->site->view('inc',$data);
	}

	public function umum_step2()
	{
		$tgl_registrasi = isset($_SESSION['tgl_registrasi']) ? $_SESSION['tgl_registrasi'] : '';
		$hari = hari_indo($tgl_registrasi);
		$poli = $this->Get_model->getKunjunganPoli(array('hari_kerja'=>$hari));
		$conp = ConDataToArray($poli,'kd_poli','nm_poli');
		$dokter = array();

		if($this->session->userdata('usia') >= 17)
		{
			unset($conp['ANA']);
		}

		if($tgl_registrasi == '')
		{
			generateReturn(base_url('bookinglansia/umum'));
		}
		else
		{
			$data = array(
				'title'		=> 'Booking Umum',
				'poli'		=> $conp,
				'hari'		=> $hari,
				'dokter'	=> $dokter,
				'inc'		=> 'lansiabumum2'
			);

			$this->site->view('inc',$data);
		}
	}

	public function batal()
	{
		global $Cf;
		$dNow = date('Y-m-d');
		$tgl = AddTglNext($dNow,$Cf->_hari_daftar,'days');

		$where = array(
				'a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),
				'a.tanggal_periksa >='=>$dNow,
				'a.tanggal_periksa <='=>$tgl,
				'a.status'=>'Belum',
				'a.kd_pj' => 'UMU'
				);
		$res = $this->Get_model->getRiwayatBooking($where);

		$data = array(
			'title'		=> 'Pembatalan Booking',
			'r'			=> $res,
			'inc'		=> 'lansiabatal'
		);
		$this->site->view('inc',$data);
	}

	public function gantipoli()
	{
		global $Cf;
		$dNow = date('Y-m-d');
		$tgl = AddTglNext($dNow,$Cf->_hari_daftar,'days');

		$where = array(
				'a.no_rkm_medis'=>$this->session->userdata('no_rkm_medis'),
				'a.tanggal_periksa >='=>$dNow,
				'a.tanggal_periksa <='=>$tgl,
				'a.status'=>'Belum',
				'a.kd_pj' => 'UMU'
				);
		$res = $this->Get_model->getRiwayatBooking($where);

		$hari = hari_indo($dNow);
		$poli = $this->Get_model->getKunjunganPoli(array('hari_kerja'=>$hari));
		$conp = ConDataToArray($poli,'kd_poli','nm_poli');
		$dokter = array();

		$data = array(
			'title'		=> 'Perubahan Poliklinik',
			'r'		=> $res,
			'poli'		=> $conp,
			'dokter'	=> $dokter,
			'inc'		=> 'lansiagantipoli'
		);
		$this->site->view('inc',$data);
	}

	public function cetak_data()
	{
		$this->load->library('Pdf');
		global $Cf;
		unset($_SESSION['tgl_registrasi']);
		$kd_poli 		= $this->uri->segment(3);
		$kd_dokter 		= $this->uri->segment(4);
		$tgl_registrasi = $this->uri->segment(5);
		$no_rkm_medis 	= $this->session->userdata('no_rkm_medis');

		$get 			= $this->Get_model->getInfoBooking(array('a.kd_poli'=>$kd_poli,'a.kd_dokter'=>$kd_dokter,'a.tanggal_periksa'=>$tgl_registrasi,'a.no_rkm_medis'=>$no_rkm_medis));

		if($get[0]->status == 'Belum' || $get[0]->status == 'Batal' || $get[0]->status == 'Terdaftar')
		{
			//Create a new PDF file
			$pdf = new FPDF('P','mm',array(125,176)); //L For Landscape / P For Portrait
			$pdf->AddPage();

			//Menambahkan Gambar
			//$pdf->Image('../foto/logo.png',10,10,-175);

			$pdf->SetFont('Arial','B',13);
			$pdf->Cell(40);
			$pdf->Cell(30,0,'RSUD KOTA DEPOK',0,0,'C');
			$pdf->SetY(12);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Jl. Raya Muchtar No.99, Sawangan Lama, Sawangan',0,0,'C');
			$pdf->Ln();
			$pdf->SetY(15);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Kota Depok, Jawa Barat 16511',0,0,'C');
			$pdf->Ln();
			$pdf->SetY(18);
			$pdf->Cell(8);
			$pdf->Cell(90,5,'Telp : (0251) 8602514',0,0,'C');
			$pdf->Ln();

			//Fields Name position
			$Y_Fields_Name_position = 35;

			//First create each Field Name
			//Gray color filling each Field Name box
			$pdf->SetFillColor(110,180,230);
			//Bold Font for Field Name
			$pdf->SetFont('Arial','B',10);
			$pdf->SetY($Y_Fields_Name_position);
			$pdf->SetX(1);
			$pdf->Cell(40,8,'Bukti Pendaftaran Online', 0 ,0 , 'L' , 0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8, 'No Urut Booking', 1 ,0 , 'C' , 1);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',15);
			$pdf->SetX(2);
			$pdf->Cell(40,8, $get[0]->no_reg, 1 ,0 , 'C' , 1);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Nomor Rekam Medik',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$no_rkm_medis,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Nama Lengkap',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$this->session->userdata('nm_lengkap'),1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Tanggal Booking',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($get[0]->tanggal_booking,true) .'  '. $get[0]->jam_booking,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Tanggal Periksa',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($tgl_registrasi,true),1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Waktu Datang',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8, tanggal_indo($get[0]->waktu_kunjungan,true) ,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Poliklinik Tujuan',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->nm_poli,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Dokter',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->nm_dokter,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Cara Bayar',1,0,'L',1);
			$pdf->SetX(42);
			$pdf->Cell(3,8,':',1,0,'C',0);
			$pdf->SetX(45);
			$pdf->Cell(78,8,$get[0]->png_jawab,1,0,'L',0);
			$pdf->Ln();
			$pdf->SetFont('Arial','',8);
			$pdf->SetX(2);
			$pdf->Cell(40,8,'Di Cetak Tanggal : ' . tanggal_indo(date('Y-m-d')), 0 ,0 , 'L' , 0);
			$pdf->SetY(128);
			$pdf->SetX(2);
			$pdf->Cell(25,6,'Catatan : Silahkan datang sesuai pada jam waktu datang yang telah ditentukan...',0,'L');

			$pdf->SetY(145);
			$pdf->SetFont('Arial','I',8);
			$pdf->Cell(0,10,'http://www.rsudreg.depok.go.id ',0,0,'C');
			$pdf->Output();
		}
		else
		{
			echo 'Data tidak bisa tercetak, disebabakan status booking menjadi Batal.';
		}
	}
}
