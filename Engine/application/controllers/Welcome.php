<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends backend_controller {

	protected $user_detail;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Get_model'));
		$this->load->library(array('recaptcha'));
	}

	public function index()
	{
		$data = array(
			'heading' => 'Error 404',
			'message' => 'Halaman tidak ditemukan.'
		);

		$this->site->view_error('error_404',$data);
	}

	public function auth()
	{
		$min_number = 1;
	    $max_number = 15;

	    // generating random numbers
	    $random_number1 = mt_rand($min_number, $max_number);
	    $random_number2 = mt_rand($min_number, $max_number);

		$data = array(
			'random_number1' => $random_number1,
			'random_number2' => $random_number2,
            'widget' => $this->recaptcha->getWidget(),
            'script' => $this->recaptcha->getScriptTag(),
        );
		$this->site->view('page/login',$data);
	}

	public function dsw()
	{
		$min_number = 1;
	    $max_number = 15;

	    // generating random numbers
	    $random_number1 = mt_rand($min_number, $max_number);
	    $random_number2 = mt_rand($min_number, $max_number);

		$data = array(
			'random_number1' => $random_number1,
			'random_number2' => $random_number2,
            'widget' => $this->recaptcha->getWidget(),
            'script' => $this->recaptcha->getScriptTag(),
        );
		$this->site->view('page/login2',$data);
		//$this->load->view('welcome_message');
	}

	public function checking()
	{
		$post = $this->input->post();
		$username = htmlentities($post['username'],ENT_QUOTES);	
		$password = htmlentities($post['password'],ENT_QUOTES);	
		$random_number1 = $post['random_number1'];
		$random_number2 = $post['random_number2'];
		$hitung = $random_number1 + $random_number2;
		$jumlah = $post['jumlah'];

		if($jumlah!=$hitung)
		{
			$_SESSION['error'] = '1';
			$_SESSION['msg']   = 'Mohon maaf hitungan anda salah';
			redirect(base_url('rsuddepok'));
		}
		else
		{
			if(isset($username) && isset($password)){
				$this->user_detail = $this->Get_model->getRecordList('pasien','*',array('no_rkm_medis'=>$username,'no_ktp'=>$password),'','',1);

				$dataLog             = array();
			    $dataLog['id_user']  = isset($username) ? $username : '';
			    $dataLog['ua']       = $_SERVER['HTTP_USER_AGENT'];
			    $dataLog['ip']       = $_SERVER['REMOTE_ADDR'];
			    $dataLog['dns']      = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			    $dataLog['date'] = date('Y-m-d H:i:s');
			    $dataLog['keterangan'] = 'Login dengan username='.$username.' dan password='.$password;
			    $this->Get_model->insertRecord('user_account_login_log',$dataLog,FALSE);

			    //$recaptcha = $post['g-recaptcha-response'];
	        	//$response = $this->recaptcha->verifyResponse($recaptcha);

	        	/*if(!isset($response['success']) || $response['success'] <> true)
	        	{
	        		redirect(base_url('welc/auth'));
	        	}
	        	else
	        	{*/
	        		if($this->user_detail)
					{
						$this->form_validation->set_message('required', '%s kosong, tolong diisi');

						$rules = $this->Get_model->auth;

						$this->form_validation->set_rules($rules);

						if($this->form_validation->run() == FALSE)
						{
							$this->Get_model->createHistory('Akun ini gagal login :'.$this->user_detail[0]->nm_pasien.'',$this->user_detail[0]->no_rkm_medis);
							$_SESSION['error'] = '1';
							$_SESSION['msg']   = '';

							redirect(base_url('rsuddepok'));
						}
						else
						{

							$login_data = array(
								'id_user' => $this->user_detail[0]->no_rkm_medis,
								'no_rkm_medis' => $post['username'],
								'nm_lengkap' => $this->user_detail[0]->nm_pasien,
								'no_ktp' => $this->user_detail[0]->no_ktp,
								'jk' => $this->user_detail[0]->jk,
								'tgl_lahir' => $this->user_detail[0]->tgl_lahir,
								'usia' => UsiaTahun($this->user_detail[0]->tgl_lahir),
								'alamat' => $this->user_detail[0]->alamat,
								'id_level' => 'Pasien',
								'status' =>  'Y',
								'logged_in' => TRUE,
							);
							
							$this->session->set_userdata($login_data);

							$this->Get_model->createHistory('Akun ini berhasil login :'.$this->session->userdata('nm_lengkap').'',$this->session->userdata('no_rkm_medis'));

							if(isset($post['remember'])){
								$expire = time() + (86400 * 7);
								set_cookie('username', $post['username'], $expire, "/");
								set_cookie('password', $post['password'], $expire, "/");
								set_cookie('no_rkm_medis',$this->user_detail[0]->no_rkm_medis, $expire, "/");
							}
							$_SESSION['error'] = '';
							$_SESSION['msg']   = 'Selamat datang';
							
							redirect(base_url('home/view'));
						}
					}
					else
					{
						$_SESSION['error'] = '1';
						$_SESSION['msg']   = 'Mohon maaf anda belum terdaftar.';
						redirect(base_url('rsuddepok'));
					}
	        	//}		
			}
			else
			{
				$_SESSION['error'] = '1';
				$_SESSION['msg']   = 'username dan password wajib diisi.';
				redirect(base_url('rsuddepok'));
			}
		}
	}

	public function logout(){
		$this->Get_model->createHistory('Akun ini berhasil logout :'.$this->session->userdata('nm_lengkap').'',$this->session->userdata('id_user'));
		$this->session->sess_destroy();
		redirect(base_url('rsuddepok'));
	}

	public function password_check($str){
    	$user_detail =  $this->user_detail[0];  
    	//echo $user_detail->password;	
    	if (@$user_detail->password == crypt($str,@$user_detail->password)){
			return TRUE;
		}
		else if(@$user_detail->password){
			$this->form_validation->set_message('password_check', 'Passwordnya Anda salah...');
			return FALSE;
		}
		else{
			$this->form_validation->set_message('password_check', 'Anda tid_userak punya akses Admin...');
			return FALSE;	
		}		
	}

	/*public function getTb($date1='',$date2='',$diagnosa='')
	{
		$kd_penyakit = explode('-',$diagnosa);
		if($kd_penyakit[0]=='A15ALL')
		{
			$kd_diag = array('A15','A15.1','A15.2','A15.3','A15.4','A15.5','A15.6','A15.7','A15.8','A15.9','A15.10','A15.11','A15.12','A15.13','A15.14','A15.15','A15.16','A15.17','A15.18','A15.19');
		}
		else
		{
			$kd_diag = $kd_penyakit;
		}
		$res = $this->Get_model->getTb($date1,$date2,$kd_diag);
			

		$data = array(
			'title' => 'Tb',
			'res' => $res
		);
		$this->site->view('page/tb',$data);
		//echo $html;
	}*/
}
