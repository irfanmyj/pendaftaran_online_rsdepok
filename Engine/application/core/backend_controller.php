<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class backend_controller extends MY_Controller{
	
	function __construct(){
		parent::__construct();

		global $Cf;

		if($Cf->_profiling == 'No') {
			$this->output->enable_profiler(FALSE);
		}else
		{
			$this->output->set_profiler_sections($Cf->_sections);
			$this->output->enable_profiler(TRUE);
		}
		
		$this->load->library(array('Site','form_validation','session'));
		$this->load->helper(array('template_helper','security','function_helper'));

		// Config template
		$this->site->side 			= 'eleco';
		$this->site->side_error 	= 'errors';
		
		$this->site->template 		= '';
		$this->site->template_error = 'html';
	}

}