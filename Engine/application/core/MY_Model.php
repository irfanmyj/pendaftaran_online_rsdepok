<?php 
defined('BASEPATH') OR exit('No direct script accsess allowed');

class MY_Model extends CI_Model{

	protected $_table_name;
	protected $_order_by;
	protected $_order_by_type;
	protected $_primary_filter = 'intval';
	protected $_primary_key;
	protected $_type;
	protected $_rules;

	function __construct(){
		parent::__construct();
	}

	public function insert($table='',$data='',$batch=FALSE)
	{
		if($batch==TRUE)
		{
			$this->db->insert_batch($table,$data);
		}
		else
		{
			$this->db->set($data);
			$this->db->insert($table);
			$id = $this->db->insert_id();
			return $id;
		}
	}

	public function insertRecord($table,$data,$debug=FALSE)
	{
		if ($debug) {
            $field = implode(',', array_keys($data));
            $value = '\'' . implode('\',\'', $data) . '\'';

            return 'INSERT INTO ' .$table . '(' . $field . ') VALUES(' . $value . ')';
        }

        $this->db->set($data);
        $this->db->insert($table);
        $id = $this->db->insert_id();
		return $id;
	}

	public function update($table='',$data, $where=array()){
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($table);
		return TRUE;
	}

	public function delete($table,$where=NULL){
		if($where){
			$this->db->where($where);
		}

		$this->db->delete($table);
	}

	public function countCustom($table='',$where=NULL)
	{
		if($where)
		{
			$this->db->where($where);
		}

		$this->db->from($table);
		return $this->db->count_all_results();	
	}

	public function getRecordList($table, $field = '', $where = '', $groupBy = '', $orderBy = '', $limit = '', $offset = NULL)
	{

		if($field)
		{
			$this->db->select($field);
		}

		if($where)
		{
			$this->db->where($where);
		}

		if($groupBy)
		{
			$this->db->group_by($groupBy);
		}

		if($orderBy)
		{
			$this->db->order_by($orderBy);
		}

		if(($limit) && ($offset)){
			$this->db->limit($limit,$offset);
		}
		else if($limit){
			$this->db->limit($limit);
		}

		return $this->db->get($table)->result();
	}

	public function createHistory($activity,$id) {
        $data                  = array();
        $data['id_user']       = $id;
        $data['activity']      = $activity;
        $data['activity_date'] = date('Y-m-d H:i:s');

        $this->db->set($data);
		$this->db->insert('user_account_log');
		$id = $this->db->insert_id();
		return $id;
    }
	
}