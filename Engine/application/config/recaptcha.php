<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LePXKMUAAAAABz7oLJClK3nLx6crKy3PtJ0lcpt';
$config['recaptcha_secret_key'] = '6LePXKMUAAAAAOWcP7qe_WcoUqMjovyq2EShdZ06';
/*$config['recaptcha_site_key'] = '6LdaUqAUAAAAAF43dTCqJ06fKP8ab1NyGZZfll1f
';
$config['recaptcha_secret_key'] = '6LdaUqAUAAAAADzssdfDLoksNhvbFk8eIjcrNbZF';*/

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
